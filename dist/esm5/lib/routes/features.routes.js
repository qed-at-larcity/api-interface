import { AppRoute } from './app.routes';
export var appFeatureRoutes = [
    {
        path: AppRoute.PRODUCT,
        loadChildren: '@quup-mvp-nx/feature-product-mvp#FeatureProductMvpModule'
    },
    {
        path: AppRoute.AUTH,
        loadChildren: '@quup-mvp-nx/shared/feature-auth#FeatureAuthModule'
    },
    /**
     * @TODO design and code the organization context for all routes below
     * this section
     */
    {
        path: AppRoute.ACCOUNT,
        loadChildren: '@quup-mvp-nx/shared/feature-account#FeatureAccountModule'
    },
    {
        path: AppRoute.EVENTS,
        loadChildren: '@quup-mvp-nx/feature-events#FeatureEventsModule'
    }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmVhdHVyZXMucm91dGVzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHF1dXAtbXZwLW54L3NoYXJlZC1hcGktaW50ZXJmYWNlLyIsInNvdXJjZXMiOlsibGliL3JvdXRlcy9mZWF0dXJlcy5yb3V0ZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUV4QyxNQUFNLENBQUMsSUFBTSxnQkFBZ0IsR0FBVztJQUN0QztRQUNFLElBQUksRUFBRSxRQUFRLENBQUMsT0FBTztRQUN0QixZQUFZLEVBQUUsMERBQTBEO0tBQ3pFO0lBQ0Q7UUFDRSxJQUFJLEVBQUUsUUFBUSxDQUFDLElBQUk7UUFDbkIsWUFBWSxFQUFFLG9EQUFvRDtLQUNuRTtJQUNEOzs7T0FHRztJQUNIO1FBQ0UsSUFBSSxFQUFFLFFBQVEsQ0FBQyxPQUFPO1FBQ3RCLFlBQVksRUFBRSwwREFBMEQ7S0FDekU7SUFDRDtRQUNFLElBQUksRUFBRSxRQUFRLENBQUMsTUFBTTtRQUNyQixZQUFZLEVBQUUsaURBQWlEO0tBQ2hFO0NBQ0YsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJvdXRlcyB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBBcHBSb3V0ZSB9IGZyb20gJy4vYXBwLnJvdXRlcyc7XG5cbmV4cG9ydCBjb25zdCBhcHBGZWF0dXJlUm91dGVzOiBSb3V0ZXMgPSBbXG4gIHtcbiAgICBwYXRoOiBBcHBSb3V0ZS5QUk9EVUNULFxuICAgIGxvYWRDaGlsZHJlbjogJ0BxdXVwLW12cC1ueC9mZWF0dXJlLXByb2R1Y3QtbXZwI0ZlYXR1cmVQcm9kdWN0TXZwTW9kdWxlJ1xuICB9LFxuICB7XG4gICAgcGF0aDogQXBwUm91dGUuQVVUSCxcbiAgICBsb2FkQ2hpbGRyZW46ICdAcXV1cC1tdnAtbngvc2hhcmVkL2ZlYXR1cmUtYXV0aCNGZWF0dXJlQXV0aE1vZHVsZSdcbiAgfSxcbiAgLyoqXG4gICAqIEBUT0RPIGRlc2lnbiBhbmQgY29kZSB0aGUgb3JnYW5pemF0aW9uIGNvbnRleHQgZm9yIGFsbCByb3V0ZXMgYmVsb3dcbiAgICogdGhpcyBzZWN0aW9uXG4gICAqL1xuICB7XG4gICAgcGF0aDogQXBwUm91dGUuQUNDT1VOVCxcbiAgICBsb2FkQ2hpbGRyZW46ICdAcXV1cC1tdnAtbngvc2hhcmVkL2ZlYXR1cmUtYWNjb3VudCNGZWF0dXJlQWNjb3VudE1vZHVsZSdcbiAgfSxcbiAge1xuICAgIHBhdGg6IEFwcFJvdXRlLkVWRU5UUyxcbiAgICBsb2FkQ2hpbGRyZW46ICdAcXV1cC1tdnAtbngvZmVhdHVyZS1ldmVudHMjRmVhdHVyZUV2ZW50c01vZHVsZSdcbiAgfVxuXTtcbiJdfQ==