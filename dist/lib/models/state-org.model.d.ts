import { OrgEntityState } from '../entities/state-org.entity';
export declare const initialOrgState: OrgEntityState;
export declare const ORG_SLICE = "organizations";
