import { InjectionToken } from '@angular/core';
export interface UITask {
    id: string;
    name: string;
    startTimeUnix?: number;
    endTimeUnix?: number;
    done?: boolean;
    error?: any;
}
export declare enum UIAlertType {
    WARNING = "alert-warning",
    SUCCESS = "alert-success",
    ERROR = "alert-danger",
    INFO = "alert-info",
    HINT = "alert-light"
}
export declare enum FormatTime {
    LONG = "dddd, MMMM Do YYYY, h:mm:ss a z",
    SHORT = "ddd, MMM Do YYYY, h:mm a"
}
export declare enum TimeZones {
    EST = "America/New_York",
    WAT = "Africa/Lagos"
}
export declare const DEFAULT_TIME_ZONE: InjectionToken<string>;
export declare const FORMAT_TIME_LONG = "dddd, MMMM Do YYYY, h:mm:ss a";
export declare const FORMAT_TIME_SHORT = "ddd, MMM Do YYYY, h:mm a";
export declare const UI_TOAST_ALERT_TYPE = UIAlertType.INFO;
export declare const UI_TOAST_DISMISS_TIMEOUT: InjectionToken<number>;
