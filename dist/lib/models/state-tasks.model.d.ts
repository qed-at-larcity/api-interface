import { QxTaskEntityState } from '../entities/state-tasks.entity';
export declare const TASKS_SLICE = "tasks";
export declare const initialTaskState: QxTaskEntityState;
