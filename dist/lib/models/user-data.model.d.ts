export declare const COLL_PATH_USER_REF = "userInfo";
export declare const COLL_PATH_USERDATA_REF = "userData";
export interface IPhoneNumber {
    number: string;
    dialCode: number;
    countryAlpha2: string;
    formattedNumber?: string;
}
export interface ITempCredentials {
    email: string;
    password?: string;
}
export interface IEMail {
    address: string;
    verified?: boolean;
}
