export interface AppSessionState {
    startTime: number;
}
export declare const initialSessionState: {
    startTime: number;
};
