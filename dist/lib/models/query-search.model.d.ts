import { CmsObjectStatus } from './cms-object.model';
export interface CmsSearchQuery {
    queryString: string;
    status?: CmsObjectStatus;
}
