import { Route } from '@angular/router';
/**
 * MUST Be implemented at the root routing level of the app to be able
 * to target the referenced app outlet
 */
export declare const WizardRouteEvent: Route;
