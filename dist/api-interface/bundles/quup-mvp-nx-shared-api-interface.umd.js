(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@ngrx/store'), require('@ngrx/entity')) :
    typeof define === 'function' && define.amd ? define('@quup-mvp-nx/shared-api-interface', ['exports', '@angular/core', '@ngrx/store', '@ngrx/entity'], factory) :
    (factory((global['quup-mvp-nx'] = global['quup-mvp-nx'] || {}, global['quup-mvp-nx']['shared-api-interface'] = {}),global.ng.core,global.store,global.entity));
}(this, (function (exports,core,store,entity) { 'use strict';

    (function (AppRoute) {
        AppRoute["AUTH"] = "auth";
        AppRoute["ACCOUNT"] = "account";
        AppRoute["PRODUCT"] = "product";
        AppRoute["EVENTS"] = "events";
        AppRoute["WIZARD_EVENT"] = "wizard/event";
    })(exports.AppRoute || (exports.AppRoute = {}));

    var appFeatureRoutes = [
        {
            path: exports.AppRoute.PRODUCT,
            loadChildren: '@quup-mvp-nx/feature-product-mvp#FeatureProductMvpModule'
        },
        {
            path: exports.AppRoute.AUTH,
            loadChildren: '@quup-mvp-nx/shared/feature-auth#FeatureAuthModule'
        },
        /**
         * @TODO design and code the organization context for all routes below
         * this section
         */
        {
            path: exports.AppRoute.ACCOUNT,
            loadChildren: '@quup-mvp-nx/shared/feature-account#FeatureAccountModule'
        },
        {
            path: exports.AppRoute.EVENTS,
            loadChildren: '@quup-mvp-nx/feature-events#FeatureEventsModule'
        }
    ];

    /**
     * MUST Be implemented at the root routing level of the app to be able
     * to target the referenced app outlet
     */
    var WizardRouteEvent = {
        path: exports.AppRoute.WIZARD_EVENT,
        outlet: 'modal',
        loadChildren: '@quup-mvp-nx/feature-events#FeatureEventsModule'
    };

    (function (CmsObjectStatus) {
        CmsObjectStatus["PUBLISHED"] = "published";
        CmsObjectStatus["DRAFT"] = "draft";
    })(exports.CmsObjectStatus || (exports.CmsObjectStatus = {}));

    var DRAFT_DOC_INDEX = 'zDraftDocument';

    (function (QxFirebaseAuthErrorCodes) {
        QxFirebaseAuthErrorCodes["EMAIL_ALREADY_EXISTS"] = "auth/email-already-exists";
    })(exports.QxFirebaseAuthErrorCodes || (exports.QxFirebaseAuthErrorCodes = {}));

    // import * as moment from 'moment-timezone';
    var TimeRecord = /** @class */ (function () {
        function TimeRecord(utc, unix) {
            if (utc === void 0) {
                utc = new Date().toISOString();
            }
            if (unix === void 0) {
                unix = new Date().getTime();
            }
            this.utc = utc;
            this.unix = unix;
        }
        // utc: string;
        // unix: number;
        TimeRecord.NOW = function (timezone) {
            if (timezone === void 0) {
                timezone = 'America/New_York';
            }
            // const now = moment(new Date()).tz(timezone);
            return TimeRecord.sanitize(new TimeRecord());
        };
        TimeRecord.sanitize = function (_a) {
            var utc = _a.utc, unix = _a.unix;
            return { utc: utc, unix: unix };
        };
        return TimeRecord;
    }());
    var DURATION_INTERVALS = [
        { label: 'minutes', value: 1 },
        { label: 'hours', value: 60 },
        { label: 'days', value: 24 * 60 },
        { label: 'weeks', value: 7 * 24 * 60 },
        { label: 'months', value: 30 * 7 * 24 * 60 }
    ];

    var COLL_PATH_USER_REF = 'userInfo';
    var COLL_PATH_USERDATA_REF = 'userData';

    var APP_CONFIG_TOKEN = new core.InjectionToken('App config');

    var Dismissable = /** @class */ (function () {
        function Dismissable() {
            /**
             * Set dismissable property to true by default
             */
            this.dismissable = true;
            /**
             * Emit instance of component being dismissed
             */
            this.dismiss = new core.EventEmitter();
        }
        return Dismissable;
    }());

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b)
                if (b.hasOwnProperty(p))
                    d[p] = b[p]; };
        return extendStatics(d, b);
    };
    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    (function (DocumentResponse) {
        DocumentResponse["ACCEPT"] = "ok";
        DocumentResponse["REJECT"] = "reject";
    })(exports.DocumentResponse || (exports.DocumentResponse = {}));
    var InteractiveDocument = /** @class */ (function (_super) {
        __extends(InteractiveDocument, _super);
        function InteractiveDocument() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            /**
             * All interactive documents are dismissable by default
             */
            _this.dismissable = true;
            /**
             * This event will be emitted when the user responds
             */
            _this.userResponse = new core.EventEmitter();
            return _this;
        }
        InteractiveDocument.propDecorators = {
            userResponse: [{ type: core.Output }]
        };
        return InteractiveDocument;
    }(Dismissable));

    (function (UIAlertType) {
        UIAlertType["WARNING"] = "alert-warning";
        UIAlertType["SUCCESS"] = "alert-success";
        UIAlertType["ERROR"] = "alert-danger";
        UIAlertType["INFO"] = "alert-info";
        UIAlertType["HINT"] = "alert-light";
    })(exports.UIAlertType || (exports.UIAlertType = {}));
    (function (FormatTime) {
        FormatTime["LONG"] = "dddd, MMMM Do YYYY, h:mm:ss a z";
        FormatTime["SHORT"] = "ddd, MMM Do YYYY, h:mm a";
    })(exports.FormatTime || (exports.FormatTime = {}));
    (function (TimeZones) {
        TimeZones["EST"] = "America/New_York";
        TimeZones["WAT"] = "Africa/Lagos"; // +01:00
    })(exports.TimeZones || (exports.TimeZones = {}));
    var DEFAULT_TIME_ZONE = new core.InjectionToken('Default TimeZone');
    var FORMAT_TIME_LONG = 'dddd, MMMM Do YYYY, h:mm:ss a';
    var FORMAT_TIME_SHORT = 'ddd, MMM Do YYYY, h:mm a';
    var UI_TOAST_ALERT_TYPE = exports.UIAlertType.INFO;
    var UI_TOAST_DISMISS_TIMEOUT = new core.InjectionToken('UI Toast Dismiss Timeout');

    var FEATURE_ACCOUNT_SLICE = 'account';
    var initialFeatureAccountState = {
        records: []
    };

    var initialFeatureAuthState = {
        agreementRecords: []
    };
    var AUTH_FEATURE_KEY = 'auth';

    var initialOrgState = {
        ids: [],
        entities: {}
    };
    var ORG_SLICE = 'organizations';

    var initialSessionState = {
        startTime: new Date().getTime()
    };

    var TASKS_SLICE = 'tasks';
    var initialTaskState = {
        taskCounter: 0,
        ids: [],
        entities: {}
    };

    var _a;
    var initialAppState = (_a = {
        auth: initialFeatureAuthState,
        session: initialSessionState
    },
        _a[TASKS_SLICE] = initialTaskState,
        _a[FEATURE_ACCOUNT_SLICE] = initialFeatureAccountState,
        _a[ORG_SLICE] = initialOrgState,
        _a);

    var FEATURE_EVENTS_ENTITY_STATE = 'event_list';
    var FEATURE_EVENTS_SLICE = 'events';
    var FEATURE_QUEUE_SLICE = 'queues';
    var COLLECTION_SLICE = new core.InjectionToken('Collection Slice');
    var compareEntityIdsAsString = function (a, b) {
        if (!a && !b) {
            throw new Error('At least 1 attribute MUST have a valid ID');
        }
        return a > b ? a.id : b.id;
    };
    var selectEntityIdAsString = function (model) {
        return model.id;
    };

    var initialCourseAliasState = {
        ids: [],
        entities: {},
        selectedCourseAliasId: null
    };
    var COURSE_ALIAS_SLICE = 'courses';

    var ErrorActionProp = /** @class */ (function () {
        function ErrorActionProp(error, length) {
            if (length === void 0) {
                length = 0;
            }
            this.error = error;
            this.length = length;
        }
        return ErrorActionProp;
    }());
    function ErrorAction(error, length) {
        if (length === void 0) {
            length = 0;
        }
        return new ErrorActionProp(error, length);
    }

    /**
     * Shared (Backend + Frontend) models
     */

    function sortCourseAliasById(e1, e2) {
        return e1.id - e2.id;
    }
    function sortCourseAliasIds(id1, id2) {
        return id1 - id2;
    }
    function reduceCourseAliasArrayToMap(map, courseAlias) {
        map[courseAlias.id] = courseAlias;
        return map;
    }
    var selectCourseAliasEntityId = function (courseAlias) {
        return courseAlias.id;
    };
    var courseAliasEntityAdapter = entity.createEntityAdapter({
        sortComparer: sortCourseAliasById,
        selectId: selectCourseAliasEntityId
    });

    var compareEventByTimeCreated = function (a, b) {
        if (!a.time_created) {
            return !b.time_created ? 0 : b.time_created.unix;
        }
        else if (!b.time_created) {
            return !a.time_created ? 0 : a.time_created.unix;
        }
        return a.time_created.unix > b.time_created.unix
            ? b.time_created.unix
            : a.time_created.unix;
    };
    var interactiveEventEntityAdapter = entity.createEntityAdapter({
        // sortComparer: compareEntityIdsAsString,
        sortComparer: compareEventByTimeCreated,
        selectId: selectEntityIdAsString
    });
    var selectInteractiveEventState = store.createFeatureSelector(FEATURE_EVENTS_SLICE);
    var _a$1 = interactiveEventEntityAdapter.getSelectors(), selectAll = _a$1.selectAll, selectEntities = _a$1.selectEntities, selectIds = _a$1.selectIds;
    var selectAllInteractiveEvents = store.createSelector(selectInteractiveEventState, function (state) { return selectAll(state); });
    var selectInteractiveEventEntities = store.createSelector(selectInteractiveEventState, function (state) { return selectEntities(state); });
    var selectInteractiveEventIds = store.createSelector(selectInteractiveEventState, function (state) { return selectIds(state); });

    var orgEntityAdapter = entity.createEntityAdapter({
        selectId: function (org) { return org.id; },
        sortComparer: function (org1, org2) { return (org1.id > org2.id ? org1.id : org2.id); }
    });

    var sortComparer = function (task1, task2) {
        return task1.id > task2.id ? task1.id : task2.id;
    };
    var ɵ0 = sortComparer;
    var selectId = function (task) { return task.id; };
    var ɵ1 = selectId;
    var taskEntityAdapter = entity.createEntityAdapter({
        sortComparer: sortComparer,
        selectId: selectId
    });

    var initialQueueEntityState = {
        ids: [],
        entities: {}
    };
    var interactiveQueueEntityAdapter = entity.createEntityAdapter({
        sortComparer: compareEntityIdsAsString,
        selectId: selectEntityIdAsString
    });

    /**
     * Generated bundle index. Do not edit.
     */

    exports.appFeatureRoutes = appFeatureRoutes;
    exports.WizardRouteEvent = WizardRouteEvent;
    exports.DRAFT_DOC_INDEX = DRAFT_DOC_INDEX;
    exports.TimeRecord = TimeRecord;
    exports.DURATION_INTERVALS = DURATION_INTERVALS;
    exports.COLL_PATH_USER_REF = COLL_PATH_USER_REF;
    exports.COLL_PATH_USERDATA_REF = COLL_PATH_USERDATA_REF;
    exports.APP_CONFIG_TOKEN = APP_CONFIG_TOKEN;
    exports.Dismissable = Dismissable;
    exports.InteractiveDocument = InteractiveDocument;
    exports.DEFAULT_TIME_ZONE = DEFAULT_TIME_ZONE;
    exports.FORMAT_TIME_LONG = FORMAT_TIME_LONG;
    exports.FORMAT_TIME_SHORT = FORMAT_TIME_SHORT;
    exports.UI_TOAST_ALERT_TYPE = UI_TOAST_ALERT_TYPE;
    exports.UI_TOAST_DISMISS_TIMEOUT = UI_TOAST_DISMISS_TIMEOUT;
    exports.initialAppState = initialAppState;
    exports.FEATURE_EVENTS_ENTITY_STATE = FEATURE_EVENTS_ENTITY_STATE;
    exports.FEATURE_EVENTS_SLICE = FEATURE_EVENTS_SLICE;
    exports.FEATURE_QUEUE_SLICE = FEATURE_QUEUE_SLICE;
    exports.COLLECTION_SLICE = COLLECTION_SLICE;
    exports.compareEntityIdsAsString = compareEntityIdsAsString;
    exports.selectEntityIdAsString = selectEntityIdAsString;
    exports.initialFeatureAuthState = initialFeatureAuthState;
    exports.AUTH_FEATURE_KEY = AUTH_FEATURE_KEY;
    exports.FEATURE_ACCOUNT_SLICE = FEATURE_ACCOUNT_SLICE;
    exports.initialFeatureAccountState = initialFeatureAccountState;
    exports.initialCourseAliasState = initialCourseAliasState;
    exports.COURSE_ALIAS_SLICE = COURSE_ALIAS_SLICE;
    exports.TASKS_SLICE = TASKS_SLICE;
    exports.initialTaskState = initialTaskState;
    exports.initialOrgState = initialOrgState;
    exports.ORG_SLICE = ORG_SLICE;
    exports.ErrorActionProp = ErrorActionProp;
    exports.ErrorAction = ErrorAction;
    exports.sortCourseAliasById = sortCourseAliasById;
    exports.sortCourseAliasIds = sortCourseAliasIds;
    exports.reduceCourseAliasArrayToMap = reduceCourseAliasArrayToMap;
    exports.selectCourseAliasEntityId = selectCourseAliasEntityId;
    exports.courseAliasEntityAdapter = courseAliasEntityAdapter;
    exports.compareEventByTimeCreated = compareEventByTimeCreated;
    exports.interactiveEventEntityAdapter = interactiveEventEntityAdapter;
    exports.selectInteractiveEventState = selectInteractiveEventState;
    exports.selectAllInteractiveEvents = selectAllInteractiveEvents;
    exports.selectInteractiveEventEntities = selectInteractiveEventEntities;
    exports.selectInteractiveEventIds = selectInteractiveEventIds;
    exports.orgEntityAdapter = orgEntityAdapter;
    exports.taskEntityAdapter = taskEntityAdapter;
    exports.ɵ0 = ɵ0;
    exports.ɵ1 = ɵ1;
    exports.initialQueueEntityState = initialQueueEntityState;
    exports.interactiveQueueEntityAdapter = interactiveQueueEntityAdapter;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=quup-mvp-nx-shared-api-interface.umd.js.map