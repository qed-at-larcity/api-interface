import { CmsUser } from './cms-object.model';
export interface UserExistsData {
    exists?: boolean;
}
export interface ResourceExistence {
    exists: boolean;
}
export interface CandidateAccessPINResponse {
    candidateAccessPIN: string;
}
export interface ApiResponse<T> {
    code?: number;
    result?: string;
    data: T[];
}
export interface CmsApiResponse<T> {
    data: T;
    code?: number | string;
    error?: any;
    [key: string]: any;
}
export interface TokenApiResponse extends CmsApiResponse<string> {
}
export interface ApiResourceExistsResponse extends CmsApiResponse<ResourceExistence> {
}
export declare type ApiCmsUser = Partial<CmsUser>;
export interface UserExistsApiResponse extends CmsApiResponse<UserExistsData> {
}
export interface UserCreateApiResponse extends CmsApiResponse<ApiCmsUser> {
}
