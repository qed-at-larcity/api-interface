import { OrgEntityState, QxCourseAliasEntityState, QxTaskEntityState } from '../entities';
import { CmsOrg } from './cms-object.model';
import { InteractiveEvent } from './interactive-event.model';
import { FeatureAccountState } from './state-feature-account.model';
import { FeatureAuthState } from './state-feature-auth.model';
import { AppSessionState } from './state-session.model';
export interface AppState {
    session: AppSessionState;
    tasks?: QxTaskEntityState;
    courses?: QxCourseAliasEntityState;
    account?: FeatureAccountState;
    auth?: FeatureAuthState;
    organizations?: OrgEntityState;
}
export declare const initialAppState: AppState;
export interface AppRouteData {
    org?: CmsOrg;
    event?: InteractiveEvent;
}
