import { Request } from 'express';
import { ApiCmsUser } from './response.model';
import { IUser } from './user.model';
import { auth } from 'firebase-admin';
export interface IResourceRequest extends Request {
    context?: string;
    allowRevokedIdToken?: boolean;
    idTokenRevoked?: boolean;
    allowAuthenticationFailure?: boolean;
    validated?: boolean;
    candidateAccessPIN?: string;
    idToken?: string;
    cmsAuthToken?: string;
    cmsUser?: ApiCmsUser;
    firebaseUser?: auth.UserRecord;
    user?: IUser;
    email?: string;
    emailVerified?: boolean;
    emailDomainValidated?: boolean;
    errors?: string[];
    [key: string]: any;
}
