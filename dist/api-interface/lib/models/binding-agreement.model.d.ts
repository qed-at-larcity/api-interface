export interface QxBindingAgreement {
    uid: string;
    description: string;
    unixTime: number;
}
