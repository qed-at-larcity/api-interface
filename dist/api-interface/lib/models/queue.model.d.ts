import { InteractiveEvent } from './interactive-event.model';
import { QxFirebaseObject } from './firebase-object.model';
export interface Queue extends QxFirebaseObject {
    id: string;
    title: string;
    description?: string;
    verified?: boolean;
    active?: boolean;
    accessCode?: string;
    placeId?: string;
}
/**
 * Legacy queue object
 */
export interface RawQueue {
    id: string;
    active: number | string;
    title: string;
    accessCode?: string;
    timeCreated?: number | string;
    timeUpdated?: number | string;
    adminRefs?: string[];
    adminRefURIs?: any[];
    event?: InteractiveEvent;
}
