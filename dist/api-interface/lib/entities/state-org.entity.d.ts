import { EntityState } from '@ngrx/entity';
import { CmsOrg } from '../models';
export interface OrgEntityState extends EntityState<CmsOrg> {
    selectedOrgId?: string;
}
export declare const orgEntityAdapter: import("@ngrx/entity").EntityAdapter<CmsOrg>;
