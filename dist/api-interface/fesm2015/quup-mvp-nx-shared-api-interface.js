import { InjectionToken, EventEmitter, Output } from '@angular/core';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { createEntityAdapter } from '@ngrx/entity';

var AppRoute;
(function (AppRoute) {
    AppRoute["AUTH"] = "auth";
    AppRoute["ACCOUNT"] = "account";
    AppRoute["PRODUCT"] = "product";
    AppRoute["EVENTS"] = "events";
    AppRoute["WIZARD_EVENT"] = "wizard/event";
})(AppRoute || (AppRoute = {}));

const appFeatureRoutes = [
    {
        path: AppRoute.PRODUCT,
        loadChildren: '@quup-mvp-nx/feature-product-mvp#FeatureProductMvpModule'
    },
    {
        path: AppRoute.AUTH,
        loadChildren: '@quup-mvp-nx/shared/feature-auth#FeatureAuthModule'
    },
    /**
     * @TODO design and code the organization context for all routes below
     * this section
     */
    {
        path: AppRoute.ACCOUNT,
        loadChildren: '@quup-mvp-nx/shared/feature-account#FeatureAccountModule'
    },
    {
        path: AppRoute.EVENTS,
        loadChildren: '@quup-mvp-nx/feature-events#FeatureEventsModule'
    }
];

/**
 * MUST Be implemented at the root routing level of the app to be able
 * to target the referenced app outlet
 */
const WizardRouteEvent = {
    path: AppRoute.WIZARD_EVENT,
    outlet: 'modal',
    loadChildren: '@quup-mvp-nx/feature-events#FeatureEventsModule'
};

var CmsObjectStatus;
(function (CmsObjectStatus) {
    CmsObjectStatus["PUBLISHED"] = "published";
    CmsObjectStatus["DRAFT"] = "draft";
})(CmsObjectStatus || (CmsObjectStatus = {}));

const DRAFT_DOC_INDEX = 'zDraftDocument';

var QxFirebaseAuthErrorCodes;
(function (QxFirebaseAuthErrorCodes) {
    QxFirebaseAuthErrorCodes["EMAIL_ALREADY_EXISTS"] = "auth/email-already-exists";
})(QxFirebaseAuthErrorCodes || (QxFirebaseAuthErrorCodes = {}));

// import * as moment from 'moment-timezone';
class TimeRecord {
    constructor(utc = new Date().toISOString(), unix = new Date().getTime()) {
        this.utc = utc;
        this.unix = unix;
    }
    // utc: string;
    // unix: number;
    static NOW(timezone = 'America/New_York') {
        // const now = moment(new Date()).tz(timezone);
        return TimeRecord.sanitize(new TimeRecord());
    }
    static sanitize({ utc, unix }) {
        return { utc, unix };
    }
}
const DURATION_INTERVALS = [
    { label: 'minutes', value: 1 },
    { label: 'hours', value: 60 },
    { label: 'days', value: 24 * 60 },
    { label: 'weeks', value: 7 * 24 * 60 },
    { label: 'months', value: 30 * 7 * 24 * 60 }
];

const COLL_PATH_USER_REF = 'userInfo';
const COLL_PATH_USERDATA_REF = 'userData';

const APP_CONFIG_TOKEN = new InjectionToken('App config');

class Dismissable {
    constructor() {
        /**
         * Set dismissable property to true by default
         */
        this.dismissable = true;
        /**
         * Emit instance of component being dismissed
         */
        this.dismiss = new EventEmitter();
    }
}

var DocumentResponse;
(function (DocumentResponse) {
    DocumentResponse["ACCEPT"] = "ok";
    DocumentResponse["REJECT"] = "reject";
})(DocumentResponse || (DocumentResponse = {}));
class InteractiveDocument extends Dismissable {
    constructor() {
        super(...arguments);
        /**
         * All interactive documents are dismissable by default
         */
        this.dismissable = true;
        /**
         * This event will be emitted when the user responds
         */
        this.userResponse = new EventEmitter();
    }
}
InteractiveDocument.propDecorators = {
    userResponse: [{ type: Output }]
};

var UIAlertType;
(function (UIAlertType) {
    UIAlertType["WARNING"] = "alert-warning";
    UIAlertType["SUCCESS"] = "alert-success";
    UIAlertType["ERROR"] = "alert-danger";
    UIAlertType["INFO"] = "alert-info";
    UIAlertType["HINT"] = "alert-light";
})(UIAlertType || (UIAlertType = {}));
var FormatTime;
(function (FormatTime) {
    FormatTime["LONG"] = "dddd, MMMM Do YYYY, h:mm:ss a z";
    FormatTime["SHORT"] = "ddd, MMM Do YYYY, h:mm a";
})(FormatTime || (FormatTime = {}));
var TimeZones;
(function (TimeZones) {
    TimeZones["EST"] = "America/New_York";
    TimeZones["WAT"] = "Africa/Lagos"; // +01:00
})(TimeZones || (TimeZones = {}));
const DEFAULT_TIME_ZONE = new InjectionToken('Default TimeZone');
const FORMAT_TIME_LONG = 'dddd, MMMM Do YYYY, h:mm:ss a';
const FORMAT_TIME_SHORT = 'ddd, MMM Do YYYY, h:mm a';
const UI_TOAST_ALERT_TYPE = UIAlertType.INFO;
const UI_TOAST_DISMISS_TIMEOUT = new InjectionToken('UI Toast Dismiss Timeout');

const FEATURE_ACCOUNT_SLICE = 'account';
const initialFeatureAccountState = {
    records: []
};

const initialFeatureAuthState = {
    agreementRecords: []
};
const AUTH_FEATURE_KEY = 'auth';

const initialOrgState = {
    ids: [],
    entities: {}
};
const ORG_SLICE = 'organizations';

const initialSessionState = {
    startTime: new Date().getTime()
};

const TASKS_SLICE = 'tasks';
const initialTaskState = {
    taskCounter: 0,
    ids: [],
    entities: {}
};

const initialAppState = {
    auth: initialFeatureAuthState,
    session: initialSessionState,
    [TASKS_SLICE]: initialTaskState,
    [FEATURE_ACCOUNT_SLICE]: initialFeatureAccountState,
    [ORG_SLICE]: initialOrgState
};

const FEATURE_EVENTS_ENTITY_STATE = 'event_list';
const FEATURE_EVENTS_SLICE = 'events';
const FEATURE_QUEUE_SLICE = 'queues';
const COLLECTION_SLICE = new InjectionToken('Collection Slice');
const compareEntityIdsAsString = (a, b) => {
    if (!a && !b) {
        throw new Error('At least 1 attribute MUST have a valid ID');
    }
    return a > b ? a.id : b.id;
};
const selectEntityIdAsString = (model) => {
    return model.id;
};

const initialCourseAliasState = {
    ids: [],
    entities: {},
    selectedCourseAliasId: null
};
const COURSE_ALIAS_SLICE = 'courses';

class ErrorActionProp {
    constructor(error, length = 0) {
        this.error = error;
        this.length = length;
    }
}
function ErrorAction(error, length = 0) {
    return new ErrorActionProp(error, length);
}

/**
 * Shared (Backend + Frontend) models
 */

function sortCourseAliasById(e1, e2) {
    return e1.id - e2.id;
}
function sortCourseAliasIds(id1, id2) {
    return id1 - id2;
}
function reduceCourseAliasArrayToMap(map, courseAlias) {
    map[courseAlias.id] = courseAlias;
    return map;
}
const selectCourseAliasEntityId = (courseAlias) => {
    return courseAlias.id;
};
const courseAliasEntityAdapter = createEntityAdapter({
    sortComparer: sortCourseAliasById,
    selectId: selectCourseAliasEntityId
});

const compareEventByTimeCreated = (a, b) => {
    if (!a.time_created) {
        return !b.time_created ? 0 : b.time_created.unix;
    }
    else if (!b.time_created) {
        return !a.time_created ? 0 : a.time_created.unix;
    }
    return a.time_created.unix > b.time_created.unix
        ? b.time_created.unix
        : a.time_created.unix;
};
const interactiveEventEntityAdapter = createEntityAdapter({
    // sortComparer: compareEntityIdsAsString,
    sortComparer: compareEventByTimeCreated,
    selectId: selectEntityIdAsString
});
const selectInteractiveEventState = createFeatureSelector(FEATURE_EVENTS_SLICE);
const { selectAll, selectEntities, selectIds } = interactiveEventEntityAdapter.getSelectors();
const selectAllInteractiveEvents = createSelector(selectInteractiveEventState, state => selectAll(state));
const selectInteractiveEventEntities = createSelector(selectInteractiveEventState, state => selectEntities(state));
const selectInteractiveEventIds = createSelector(selectInteractiveEventState, state => selectIds(state));

const orgEntityAdapter = createEntityAdapter({
    selectId: org => org.id,
    sortComparer: (org1, org2) => (org1.id > org2.id ? org1.id : org2.id)
});

const sortComparer = (task1, task2) => {
    return task1.id > task2.id ? task1.id : task2.id;
};
const ɵ0 = sortComparer;
const selectId = task => task.id;
const ɵ1 = selectId;
const taskEntityAdapter = createEntityAdapter({
    sortComparer,
    selectId
});

const initialQueueEntityState = {
    ids: [],
    entities: {}
};
const interactiveQueueEntityAdapter = createEntityAdapter({
    sortComparer: compareEntityIdsAsString,
    selectId: selectEntityIdAsString
});

/**
 * Generated bundle index. Do not edit.
 */

export { AppRoute, appFeatureRoutes, WizardRouteEvent, CmsObjectStatus, DRAFT_DOC_INDEX, QxFirebaseAuthErrorCodes, TimeRecord, DURATION_INTERVALS, COLL_PATH_USER_REF, COLL_PATH_USERDATA_REF, APP_CONFIG_TOKEN, Dismissable, DocumentResponse, InteractiveDocument, UIAlertType, FormatTime, TimeZones, DEFAULT_TIME_ZONE, FORMAT_TIME_LONG, FORMAT_TIME_SHORT, UI_TOAST_ALERT_TYPE, UI_TOAST_DISMISS_TIMEOUT, initialAppState, FEATURE_EVENTS_ENTITY_STATE, FEATURE_EVENTS_SLICE, FEATURE_QUEUE_SLICE, COLLECTION_SLICE, compareEntityIdsAsString, selectEntityIdAsString, initialFeatureAuthState, AUTH_FEATURE_KEY, FEATURE_ACCOUNT_SLICE, initialFeatureAccountState, initialCourseAliasState, COURSE_ALIAS_SLICE, TASKS_SLICE, initialTaskState, initialOrgState, ORG_SLICE, ErrorActionProp, ErrorAction, sortCourseAliasById, sortCourseAliasIds, reduceCourseAliasArrayToMap, selectCourseAliasEntityId, courseAliasEntityAdapter, compareEventByTimeCreated, interactiveEventEntityAdapter, selectInteractiveEventState, selectAllInteractiveEvents, selectInteractiveEventEntities, selectInteractiveEventIds, orgEntityAdapter, taskEntityAdapter, ɵ0, ɵ1, initialQueueEntityState, interactiveQueueEntityAdapter };

//# sourceMappingURL=quup-mvp-nx-shared-api-interface.js.map