import { createEntityAdapter } from '@ngrx/entity';
const sortComparer = (task1, task2) => {
    return task1.id > task2.id ? task1.id : task2.id;
};
const ɵ0 = sortComparer;
const selectId = task => task.id;
const ɵ1 = selectId;
export const taskEntityAdapter = createEntityAdapter({
    sortComparer,
    selectId
});
export { ɵ0, ɵ1 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGUtdGFza3MuZW50aXR5LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHF1dXAtbXZwLW54L3NoYXJlZC1hcGktaW50ZXJmYWNlLyIsInNvdXJjZXMiOlsibGliL2VudGl0aWVzL3N0YXRlLXRhc2tzLmVudGl0eS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQWUsbUJBQW1CLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFTaEUsTUFBTSxZQUFZLEdBQXdCLENBQUMsS0FBYSxFQUFFLEtBQWEsRUFBRSxFQUFFO0lBQ3pFLE9BQU8sS0FBSyxDQUFDLEVBQUUsR0FBRyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDO0FBQ25ELENBQUMsQ0FBQzs7QUFFRixNQUFNLFFBQVEsR0FBdUIsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDOztBQUVyRCxNQUFNLENBQUMsTUFBTSxpQkFBaUIsR0FBRyxtQkFBbUIsQ0FBUztJQUMzRCxZQUFZO0lBQ1osUUFBUTtDQUNULENBQUMsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEVudGl0eVN0YXRlLCBjcmVhdGVFbnRpdHlBZGFwdGVyIH0gZnJvbSAnQG5ncngvZW50aXR5JztcbmltcG9ydCB7IFVJVGFzayB9IGZyb20gJ0BxdXVwLW12cC1ueC9zaGFyZWQvYXBpLWludGVyZmFjZSc7XG5pbXBvcnQgeyBDb21wYXJlclN0ciwgSWRTZWxlY3RvciB9IGZyb20gJ0BuZ3J4L2VudGl0eS9zcmMvbW9kZWxzJztcblxuZXhwb3J0IGludGVyZmFjZSBReFRhc2tFbnRpdHlTdGF0ZSBleHRlbmRzIEVudGl0eVN0YXRlPFVJVGFzaz4ge1xuICBjdXJyZW50VGFza0lkPzogc3RyaW5nO1xuICB0YXNrQ291bnRlcjogbnVtYmVyO1xufVxuXG5jb25zdCBzb3J0Q29tcGFyZXI6IENvbXBhcmVyU3RyPFVJVGFzaz4gPSAodGFzazE6IFVJVGFzaywgdGFzazI6IFVJVGFzaykgPT4ge1xuICByZXR1cm4gdGFzazEuaWQgPiB0YXNrMi5pZCA/IHRhc2sxLmlkIDogdGFzazIuaWQ7XG59O1xuXG5jb25zdCBzZWxlY3RJZDogSWRTZWxlY3RvcjxVSVRhc2s+ID0gdGFzayA9PiB0YXNrLmlkO1xuXG5leHBvcnQgY29uc3QgdGFza0VudGl0eUFkYXB0ZXIgPSBjcmVhdGVFbnRpdHlBZGFwdGVyPFVJVGFzaz4oe1xuICBzb3J0Q29tcGFyZXIsXG4gIHNlbGVjdElkXG59KTtcbiJdfQ==