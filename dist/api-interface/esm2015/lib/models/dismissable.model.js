import { EventEmitter } from '@angular/core';
export class Dismissable {
    constructor() {
        /**
         * Set dismissable property to true by default
         */
        this.dismissable = true;
        /**
         * Emit instance of component being dismissed
         */
        this.dismiss = new EventEmitter();
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlzbWlzc2FibGUubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcXV1cC1tdnAtbngvc2hhcmVkLWFwaS1pbnRlcmZhY2UvIiwic291cmNlcyI6WyJsaWIvbW9kZWxzL2Rpc21pc3NhYmxlLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFN0MsTUFBTSxPQUFnQixXQUFXO0lBQWpDO1FBQ0U7O1dBRUc7UUFDSCxnQkFBVyxHQUFHLElBQUksQ0FBQztRQUVuQjs7V0FFRztRQUNILFlBQU8sR0FBRyxJQUFJLFlBQVksRUFBSyxDQUFDO0lBQ2xDLENBQUM7Q0FBQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgRGlzbWlzc2FibGU8VD4ge1xuICAvKipcbiAgICogU2V0IGRpc21pc3NhYmxlIHByb3BlcnR5IHRvIHRydWUgYnkgZGVmYXVsdFxuICAgKi9cbiAgZGlzbWlzc2FibGUgPSB0cnVlO1xuXG4gIC8qKlxuICAgKiBFbWl0IGluc3RhbmNlIG9mIGNvbXBvbmVudCBiZWluZyBkaXNtaXNzZWRcbiAgICovXG4gIGRpc21pc3MgPSBuZXcgRXZlbnRFbWl0dGVyPFQ+KCk7XG59XG4iXX0=