export interface CmsContext {
    apiEndpoint?: string;
}
export interface AppSecrets {
    cmsAdminToken?: string | null;
    [key: string]: any;
}
export interface FirebaseConfig {
    apiKey: string;
    authDomain: string;
    databaseURL: string;
    projectId: string;
    storageBucket: string;
    messagingSenderId?: string;
    appId?: string;
}
export interface ReCaptchaConfig {
    siteKey: string;
    secretKey?: string;
}
export interface AppContext {
    production: boolean;
    apiEndpoint: string;
    secrets: AppSecrets;
    firebase?: FirebaseConfig;
    firebaseAdminKeyFile?: string | null;
    cms?: CmsContext;
    reCaptcha?: ReCaptchaConfig;
    cmsEndpoint?: string;
    cloudFxnsEndpoint?: string;
    microServiceEndpoint?: string;
    apiPort?: number;
}
