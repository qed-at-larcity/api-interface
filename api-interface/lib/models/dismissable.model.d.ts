import { EventEmitter } from '@angular/core';
export declare abstract class Dismissable<T> {
    /**
     * Set dismissable property to true by default
     */
    dismissable: boolean;
    /**
     * Emit instance of component being dismissed
     */
    dismiss: EventEmitter<T>;
}
