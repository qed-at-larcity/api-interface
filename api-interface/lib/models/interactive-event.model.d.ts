import { TimeRecord } from './time-record.model';
import { Queue } from './queue.model';
import { GenericFormInputOption } from './input-options.model';
import { QxFirebaseObject } from './firebase-object.model';
import { DocumentReference } from '@angular/fire/firestore';
/** @TODO undocumented */
export interface InteractiveEvent extends QxFirebaseObject {
    title: string;
    description?: string;
    course_alias?: number;
    course_hint?: string;
    queueRefById?: string;
    queueRef?: DocumentReference;
    queue?: Queue;
    duration?: number;
    duration_interval?: GenericFormInputOption;
    time_start?: TimeRecord;
    time_end?: TimeRecord;
    public?: boolean;
}
