import { InjectionToken } from '@angular/core';
import { ComparerStr, IdSelectorStr } from '@ngrx/entity/src/models';
import { QxFirebaseObject } from './firebase-object.model';
export declare const FEATURE_EVENTS_ENTITY_STATE = "event_list";
export declare const FEATURE_EVENTS_SLICE = "events";
export declare const FEATURE_QUEUE_SLICE = "queues";
export declare const COLLECTION_SLICE: InjectionToken<string>;
export declare const compareEntityIdsAsString: ComparerStr<QxFirebaseObject>;
export declare const selectEntityIdAsString: IdSelectorStr<QxFirebaseObject>;
