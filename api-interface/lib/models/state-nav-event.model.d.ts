import { InteractiveEvent } from './interactive-event.model';
export interface SelectedInteractiveEventState {
    event: InteractiveEvent;
}
