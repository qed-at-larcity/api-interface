import { IEMail, ITempCredentials } from './user-data.model';
export interface IUserRef {
    emails: string[];
    name: string;
    uid?: string;
    roles?: string[];
    picture?: string;
    timeCreated: number;
    [prop: string]: any;
}
export interface IUser {
    cmsId?: number;
    uid?: string;
    displayName?: string;
    photoURL?: string;
    emails?: IEMail[];
    tempCredentials?: ITempCredentials;
    countryAlpha2?: string;
    [aKey: string]: any;
}
export interface IUserInput extends IUser {
    firstName?: string;
    lastName?: string;
    email?: string;
    password?: string;
    phoneNumber?: string;
    countryAlpha2?: string;
}
