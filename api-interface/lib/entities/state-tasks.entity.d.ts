import { EntityState } from '@ngrx/entity';
import { UITask } from '@quup-mvp-nx/shared/api-interface';
export interface QxTaskEntityState extends EntityState<UITask> {
    currentTaskId?: string;
    taskCounter: number;
}
export declare const taskEntityAdapter: import("@ngrx/entity").EntityAdapter<UITask>;
