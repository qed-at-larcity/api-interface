import core from '@angular/core';
import tslib from 'tslib';
import store from '@ngrx/store';
import entity from '@ngrx/entity';

function unwrapExports (x) {
	return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
}

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var app_routes = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });
var AppRoute;
(function (AppRoute) {
    AppRoute["AUTH"] = "auth";
    AppRoute["ACCOUNT"] = "account";
    AppRoute["PRODUCT"] = "product";
    AppRoute["EVENTS"] = "events";
    AppRoute["WIZARD_EVENT"] = "wizard/event";
})(AppRoute = exports.AppRoute || (exports.AppRoute = {}));

});

unwrapExports(app_routes);
var app_routes_1 = app_routes.AppRoute;

var features_routes = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });

exports.appFeatureRoutes = [
    {
        path: app_routes.AppRoute.PRODUCT,
        loadChildren: '@quup-mvp-nx/feature-product-mvp#FeatureProductMvpModule'
    },
    {
        path: app_routes.AppRoute.AUTH,
        loadChildren: '@quup-mvp-nx/shared/feature-auth#FeatureAuthModule'
    },
    /**
     * @TODO design and code the organization context for all routes below
     * this section
     */
    {
        path: app_routes.AppRoute.ACCOUNT,
        loadChildren: '@quup-mvp-nx/shared/feature-account#FeatureAccountModule'
    },
    {
        path: app_routes.AppRoute.EVENTS,
        loadChildren: '@quup-mvp-nx/feature-events#FeatureEventsModule'
    }
];

});

unwrapExports(features_routes);
var features_routes_1 = features_routes.appFeatureRoutes;

var wizard_routes = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });

/**
 * MUST Be implemented at the root routing level of the app to be able
 * to target the referenced app outlet
 */
exports.WizardRouteEvent = {
    path: app_routes.AppRoute.WIZARD_EVENT,
    outlet: 'modal',
    loadChildren: '@quup-mvp-nx/feature-events#FeatureEventsModule'
};

});

unwrapExports(wizard_routes);
var wizard_routes_1 = wizard_routes.WizardRouteEvent;

var routes = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });

tslib.__exportStar(app_routes, exports);
tslib.__exportStar(features_routes, exports);
tslib.__exportStar(wizard_routes, exports);

});

unwrapExports(routes);

var cmsObject_model = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });
var CmsObjectStatus;
(function (CmsObjectStatus) {
    CmsObjectStatus["PUBLISHED"] = "published";
    CmsObjectStatus["DRAFT"] = "draft";
})(CmsObjectStatus = exports.CmsObjectStatus || (exports.CmsObjectStatus = {}));

});

unwrapExports(cmsObject_model);
var cmsObject_model_1 = cmsObject_model.CmsObjectStatus;

var firebaseObject_model = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.DRAFT_DOC_INDEX = 'zDraftDocument';

});

unwrapExports(firebaseObject_model);
var firebaseObject_model_1 = firebaseObject_model.DRAFT_DOC_INDEX;

var firebaseAuthError_model = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });
var QxFirebaseAuthErrorCodes;
(function (QxFirebaseAuthErrorCodes) {
    QxFirebaseAuthErrorCodes["EMAIL_ALREADY_EXISTS"] = "auth/email-already-exists";
})(QxFirebaseAuthErrorCodes = exports.QxFirebaseAuthErrorCodes || (exports.QxFirebaseAuthErrorCodes = {}));

});

unwrapExports(firebaseAuthError_model);
var firebaseAuthError_model_1 = firebaseAuthError_model.QxFirebaseAuthErrorCodes;

var timeRecord_model = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });
// import * as moment from 'moment-timezone';
class TimeRecord {
    constructor(utc = new Date().toISOString(), unix = new Date().getTime()) {
        this.utc = utc;
        this.unix = unix;
    }
    // utc: string;
    // unix: number;
    static NOW(timezone = 'America/New_York') {
        // const now = moment(new Date()).tz(timezone);
        return TimeRecord.sanitize(new TimeRecord());
    }
    static sanitize({ utc, unix }) {
        return { utc, unix };
    }
}
exports.TimeRecord = TimeRecord;
exports.DURATION_INTERVALS = [
    { label: 'minutes', value: 1 },
    { label: 'hours', value: 60 },
    { label: 'days', value: 24 * 60 },
    { label: 'weeks', value: 7 * 24 * 60 },
    { label: 'months', value: 30 * 7 * 24 * 60 }
];

});

unwrapExports(timeRecord_model);
var timeRecord_model_1 = timeRecord_model.TimeRecord;
var timeRecord_model_2 = timeRecord_model.DURATION_INTERVALS;

var userData_model = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.COLL_PATH_USER_REF = 'userInfo';
exports.COLL_PATH_USERDATA_REF = 'userData';

});

unwrapExports(userData_model);
var userData_model_1 = userData_model.COLL_PATH_USER_REF;
var userData_model_2 = userData_model.COLL_PATH_USERDATA_REF;

var environment_model = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });

exports.APP_CONFIG_TOKEN = new core.InjectionToken('App config');

});

unwrapExports(environment_model);
var environment_model_1 = environment_model.APP_CONFIG_TOKEN;

var dismissable_model = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });

class Dismissable {
    constructor() {
        /**
         * Set dismissable property to true by default
         */
        this.dismissable = true;
        /**
         * Emit instance of component being dismissed
         */
        this.dismiss = new core.EventEmitter();
    }
}
exports.Dismissable = Dismissable;

});

unwrapExports(dismissable_model);
var dismissable_model_1 = dismissable_model.Dismissable;

var interactiveDoc_model = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });


var DocumentResponse;
(function (DocumentResponse) {
    DocumentResponse["ACCEPT"] = "ok";
    DocumentResponse["REJECT"] = "reject";
})(DocumentResponse = exports.DocumentResponse || (exports.DocumentResponse = {}));
class InteractiveDocument extends dismissable_model.Dismissable {
    constructor() {
        super(...arguments);
        /**
         * All interactive documents are dismissable by default
         */
        this.dismissable = true;
        /**
         * This event will be emitted when the user responds
         */
        this.userResponse = new core.EventEmitter();
    }
}
InteractiveDocument.propDecorators = {
    userResponse: [{ type: core.Output }]
};
exports.InteractiveDocument = InteractiveDocument;

});

unwrapExports(interactiveDoc_model);
var interactiveDoc_model_1 = interactiveDoc_model.DocumentResponse;
var interactiveDoc_model_2 = interactiveDoc_model.InteractiveDocument;

var ui_model = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });

var UIAlertType;
(function (UIAlertType) {
    UIAlertType["WARNING"] = "alert-warning";
    UIAlertType["SUCCESS"] = "alert-success";
    UIAlertType["ERROR"] = "alert-danger";
    UIAlertType["INFO"] = "alert-info";
    UIAlertType["HINT"] = "alert-light";
})(UIAlertType = exports.UIAlertType || (exports.UIAlertType = {}));
var FormatTime;
(function (FormatTime) {
    FormatTime["LONG"] = "dddd, MMMM Do YYYY, h:mm:ss a z";
    FormatTime["SHORT"] = "ddd, MMM Do YYYY, h:mm a";
})(FormatTime = exports.FormatTime || (exports.FormatTime = {}));
var TimeZones;
(function (TimeZones) {
    TimeZones["EST"] = "America/New_York";
    TimeZones["WAT"] = "Africa/Lagos"; // +01:00
})(TimeZones = exports.TimeZones || (exports.TimeZones = {}));
exports.DEFAULT_TIME_ZONE = new core.InjectionToken('Default TimeZone');
exports.FORMAT_TIME_LONG = 'dddd, MMMM Do YYYY, h:mm:ss a';
exports.FORMAT_TIME_SHORT = 'ddd, MMM Do YYYY, h:mm a';
exports.UI_TOAST_ALERT_TYPE = UIAlertType.INFO;
exports.UI_TOAST_DISMISS_TIMEOUT = new core.InjectionToken('UI Toast Dismiss Timeout');
exports.MODAL_OVERLAY_REF = new core.InjectionToken('Modal OverlayRef');

});

unwrapExports(ui_model);
var ui_model_1 = ui_model.UIAlertType;
var ui_model_2 = ui_model.FormatTime;
var ui_model_3 = ui_model.TimeZones;
var ui_model_4 = ui_model.DEFAULT_TIME_ZONE;
var ui_model_5 = ui_model.FORMAT_TIME_LONG;
var ui_model_6 = ui_model.FORMAT_TIME_SHORT;
var ui_model_7 = ui_model.UI_TOAST_ALERT_TYPE;
var ui_model_8 = ui_model.UI_TOAST_DISMISS_TIMEOUT;
var ui_model_9 = ui_model.MODAL_OVERLAY_REF;

var stateFeatureAccount_model = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.FEATURE_ACCOUNT_SLICE = 'account';
exports.initialFeatureAccountState = {
    records: []
};

});

unwrapExports(stateFeatureAccount_model);
var stateFeatureAccount_model_1 = stateFeatureAccount_model.FEATURE_ACCOUNT_SLICE;
var stateFeatureAccount_model_2 = stateFeatureAccount_model.initialFeatureAccountState;

var stateFeatureAuth_model = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.initialFeatureAuthState = {
    agreementRecords: []
};
exports.AUTH_FEATURE_KEY = 'auth';

});

unwrapExports(stateFeatureAuth_model);
var stateFeatureAuth_model_1 = stateFeatureAuth_model.initialFeatureAuthState;
var stateFeatureAuth_model_2 = stateFeatureAuth_model.AUTH_FEATURE_KEY;

var stateOrg_model = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.initialOrgState = {
    ids: [],
    entities: {}
};
exports.ORG_SLICE = 'organizations';

});

unwrapExports(stateOrg_model);
var stateOrg_model_1 = stateOrg_model.initialOrgState;
var stateOrg_model_2 = stateOrg_model.ORG_SLICE;

var stateSession_model = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.initialSessionState = {
    startTime: new Date().getTime()
};

});

unwrapExports(stateSession_model);
var stateSession_model_1 = stateSession_model.initialSessionState;

var stateTasks_model = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.TASKS_SLICE = 'tasks';
exports.initialTaskState = {
    taskCounter: 0,
    ids: [],
    entities: {}
};

});

unwrapExports(stateTasks_model);
var stateTasks_model_1 = stateTasks_model.TASKS_SLICE;
var stateTasks_model_2 = stateTasks_model.initialTaskState;

var stateApp_model = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });





exports.initialAppState = {
    auth: stateFeatureAuth_model.initialFeatureAuthState,
    session: stateSession_model.initialSessionState,
    [stateTasks_model.TASKS_SLICE]: stateTasks_model.initialTaskState,
    [stateFeatureAccount_model.FEATURE_ACCOUNT_SLICE]: stateFeatureAccount_model.initialFeatureAccountState,
    [stateOrg_model.ORG_SLICE]: stateOrg_model.initialOrgState
};

});

unwrapExports(stateApp_model);
var stateApp_model_1 = stateApp_model.initialAppState;

var stateFeature_model = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });

exports.FEATURE_EVENTS_ENTITY_STATE = 'event_list';
exports.FEATURE_EVENTS_SLICE = 'events';
exports.FEATURE_QUEUE_SLICE = 'queues';
exports.COLLECTION_SLICE = new core.InjectionToken('Collection Slice');
exports.compareEntityIdsAsString = (a, b) => {
    if (!a && !b) {
        throw new Error('At least 1 attribute MUST have a valid ID');
    }
    return a > b ? a.id : b.id;
};
exports.selectEntityIdAsString = (model) => {
    return model.id;
};

});

unwrapExports(stateFeature_model);
var stateFeature_model_1 = stateFeature_model.FEATURE_EVENTS_ENTITY_STATE;
var stateFeature_model_2 = stateFeature_model.FEATURE_EVENTS_SLICE;
var stateFeature_model_3 = stateFeature_model.FEATURE_QUEUE_SLICE;
var stateFeature_model_4 = stateFeature_model.COLLECTION_SLICE;
var stateFeature_model_5 = stateFeature_model.compareEntityIdsAsString;
var stateFeature_model_6 = stateFeature_model.selectEntityIdAsString;

var stateCourses_model = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.initialCourseAliasState = {
    ids: [],
    entities: {},
    selectedCourseAliasId: null
};
exports.COURSE_ALIAS_SLICE = 'courses';

});

unwrapExports(stateCourses_model);
var stateCourses_model_1 = stateCourses_model.initialCourseAliasState;
var stateCourses_model_2 = stateCourses_model.COURSE_ALIAS_SLICE;

var ngrx_model = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });
class ErrorActionProp {
    constructor(error, length = 0) {
        this.error = error;
        this.length = length;
    }
}
exports.ErrorActionProp = ErrorActionProp;
function ErrorAction(error, length = 0) {
    return new ErrorActionProp(error, length);
}
exports.ErrorAction = ErrorAction;

});

unwrapExports(ngrx_model);
var ngrx_model_1 = ngrx_model.ErrorActionProp;
var ngrx_model_2 = ngrx_model.ErrorAction;

var models = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });

/**
 * Shared (Backend + Frontend) models
 */
tslib.__exportStar(cmsObject_model, exports);
tslib.__exportStar(firebaseObject_model, exports);
tslib.__exportStar(firebaseAuthError_model, exports);
tslib.__exportStar(timeRecord_model, exports);
tslib.__exportStar(userData_model, exports);
/**
 * Frontend models
 */
tslib.__exportStar(environment_model, exports);
tslib.__exportStar(dismissable_model, exports);
tslib.__exportStar(interactiveDoc_model, exports);
tslib.__exportStar(ui_model, exports);
tslib.__exportStar(stateApp_model, exports);
tslib.__exportStar(stateFeature_model, exports);
tslib.__exportStar(stateFeatureAuth_model, exports);
tslib.__exportStar(stateFeatureAccount_model, exports);
tslib.__exportStar(stateCourses_model, exports);
tslib.__exportStar(stateTasks_model, exports);
tslib.__exportStar(stateOrg_model, exports);
// export * from './state-queue.model';
tslib.__exportStar(ngrx_model, exports);

});

unwrapExports(models);

var stateCourseAlias_entity = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });

function sortCourseAliasById(e1, e2) {
    return e1.id - e2.id;
}
exports.sortCourseAliasById = sortCourseAliasById;
function sortCourseAliasIds(id1, id2) {
    return id1 - id2;
}
exports.sortCourseAliasIds = sortCourseAliasIds;
function reduceCourseAliasArrayToMap(map, courseAlias) {
    map[courseAlias.id] = courseAlias;
    return map;
}
exports.reduceCourseAliasArrayToMap = reduceCourseAliasArrayToMap;
exports.selectCourseAliasEntityId = (courseAlias) => {
    return courseAlias.id;
};
exports.courseAliasEntityAdapter = entity.createEntityAdapter({
    sortComparer: sortCourseAliasById,
    selectId: exports.selectCourseAliasEntityId
});

});

unwrapExports(stateCourseAlias_entity);
var stateCourseAlias_entity_1 = stateCourseAlias_entity.sortCourseAliasById;
var stateCourseAlias_entity_2 = stateCourseAlias_entity.sortCourseAliasIds;
var stateCourseAlias_entity_3 = stateCourseAlias_entity.reduceCourseAliasArrayToMap;
var stateCourseAlias_entity_4 = stateCourseAlias_entity.selectCourseAliasEntityId;
var stateCourseAlias_entity_5 = stateCourseAlias_entity.courseAliasEntityAdapter;

var stateInteractiveEvent_entity = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });



exports.compareEventByTimeCreated = (a, b) => {
    if (!a.time_created) {
        return !b.time_created ? 0 : b.time_created.unix;
    }
    else if (!b.time_created) {
        return !a.time_created ? 0 : a.time_created.unix;
    }
    return a.time_created.unix > b.time_created.unix
        ? b.time_created.unix
        : a.time_created.unix;
};
exports.interactiveEventEntityAdapter = entity.createEntityAdapter({
    // sortComparer: compareEntityIdsAsString,
    sortComparer: exports.compareEventByTimeCreated,
    selectId: models.selectEntityIdAsString
});
exports.selectInteractiveEventState = store.createFeatureSelector(models.FEATURE_EVENTS_SLICE);
const { selectAll, selectEntities, selectIds } = exports.interactiveEventEntityAdapter.getSelectors();
exports.selectAllInteractiveEvents = store.createSelector(exports.selectInteractiveEventState, state => selectAll(state));
exports.selectInteractiveEventEntities = store.createSelector(exports.selectInteractiveEventState, state => selectEntities(state));
exports.selectInteractiveEventIds = store.createSelector(exports.selectInteractiveEventState, state => selectIds(state));

});

unwrapExports(stateInteractiveEvent_entity);
var stateInteractiveEvent_entity_1 = stateInteractiveEvent_entity.compareEventByTimeCreated;
var stateInteractiveEvent_entity_2 = stateInteractiveEvent_entity.interactiveEventEntityAdapter;
var stateInteractiveEvent_entity_3 = stateInteractiveEvent_entity.selectInteractiveEventState;
var stateInteractiveEvent_entity_4 = stateInteractiveEvent_entity.selectAllInteractiveEvents;
var stateInteractiveEvent_entity_5 = stateInteractiveEvent_entity.selectInteractiveEventEntities;
var stateInteractiveEvent_entity_6 = stateInteractiveEvent_entity.selectInteractiveEventIds;

var stateOrg_entity = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });

exports.orgEntityAdapter = entity.createEntityAdapter({
    selectId: org => org.id,
    sortComparer: (org1, org2) => (org1.id > org2.id ? org1.id : org2.id)
});

});

unwrapExports(stateOrg_entity);
var stateOrg_entity_1 = stateOrg_entity.orgEntityAdapter;

var stateTasks_entity = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });

const sortComparer = (task1, task2) => {
    return task1.id > task2.id ? task1.id : task2.id;
};
const ɵ0 = sortComparer;
exports.ɵ0 = ɵ0;
const selectId = task => task.id;
const ɵ1 = selectId;
exports.ɵ1 = ɵ1;
exports.taskEntityAdapter = entity.createEntityAdapter({
    sortComparer,
    selectId
});

});

unwrapExports(stateTasks_entity);
var stateTasks_entity_1 = stateTasks_entity.taskEntityAdapter;

var stateQueue_entity = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });


exports.initialQueueEntityState = {
    ids: [],
    entities: {}
};
exports.interactiveQueueEntityAdapter = entity.createEntityAdapter({
    sortComparer: models.compareEntityIdsAsString,
    selectId: models.selectEntityIdAsString
});

});

unwrapExports(stateQueue_entity);
var stateQueue_entity_1 = stateQueue_entity.initialQueueEntityState;
var stateQueue_entity_2 = stateQueue_entity.interactiveQueueEntityAdapter;

var entities = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });

tslib.__exportStar(stateCourseAlias_entity, exports);
tslib.__exportStar(stateInteractiveEvent_entity, exports);
tslib.__exportStar(stateOrg_entity, exports);
tslib.__exportStar(stateTasks_entity, exports);
tslib.__exportStar(stateQueue_entity, exports);

});

unwrapExports(entities);

var esm2015 = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });

tslib.__exportStar(routes, exports);
tslib.__exportStar(models, exports);
tslib.__exportStar(entities, exports);

});

unwrapExports(esm2015);

var quupMvpNxSharedApiInterface = createCommonjsModule(function (module, exports) {
/**
 * Generated bundle index. Do not edit.
 */
Object.defineProperty(exports, "__esModule", { value: true });

tslib.__exportStar(esm2015, exports);

});

var quupMvpNxSharedApiInterface$1 = unwrapExports(quupMvpNxSharedApiInterface);

export default quupMvpNxSharedApiInterface$1;

//# sourceMappingURL=quup-mvp-nx-shared-api-interface.js.map