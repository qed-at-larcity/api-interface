"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AppRoute;
(function (AppRoute) {
    AppRoute["AUTH"] = "auth";
    AppRoute["ACCOUNT"] = "account";
    AppRoute["PRODUCT"] = "product";
    AppRoute["EVENTS"] = "events";
    AppRoute["WIZARD_EVENT"] = "wizard/event";
})(AppRoute = exports.AppRoute || (exports.AppRoute = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLnJvdXRlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BxdXVwLW12cC1ueC9zaGFyZWQtYXBpLWludGVyZmFjZS8iLCJzb3VyY2VzIjpbImxpYi9yb3V0ZXMvYXBwLnJvdXRlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLElBQVksUUFNWDtBQU5ELFdBQVksUUFBUTtJQUNsQix5QkFBYSxDQUFBO0lBQ2IsK0JBQW1CLENBQUE7SUFDbkIsK0JBQW1CLENBQUE7SUFDbkIsNkJBQWlCLENBQUE7SUFDakIseUNBQTZCLENBQUE7QUFDL0IsQ0FBQyxFQU5XLFFBQVEsR0FBUixnQkFBUSxLQUFSLGdCQUFRLFFBTW5CIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGVudW0gQXBwUm91dGUge1xuICBBVVRIID0gJ2F1dGgnLFxuICBBQ0NPVU5UID0gJ2FjY291bnQnLFxuICBQUk9EVUNUID0gJ3Byb2R1Y3QnLFxuICBFVkVOVFMgPSAnZXZlbnRzJyxcbiAgV0laQVJEX0VWRU5UID0gJ3dpemFyZC9ldmVudCdcbn1cbiJdfQ==