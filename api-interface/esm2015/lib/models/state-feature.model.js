"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
exports.FEATURE_EVENTS_ENTITY_STATE = 'event_list';
exports.FEATURE_EVENTS_SLICE = 'events';
exports.FEATURE_QUEUE_SLICE = 'queues';
exports.COLLECTION_SLICE = new core_1.InjectionToken('Collection Slice');
exports.compareEntityIdsAsString = (a, b) => {
    if (!a && !b) {
        throw new Error('At least 1 attribute MUST have a valid ID');
    }
    return a > b ? a.id : b.id;
};
exports.selectEntityIdAsString = (model) => {
    return model.id;
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGUtZmVhdHVyZS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BxdXVwLW12cC1ueC9zaGFyZWQtYXBpLWludGVyZmFjZS8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvc3RhdGUtZmVhdHVyZS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHdDQUErQztBQUlsQyxRQUFBLDJCQUEyQixHQUFHLFlBQVksQ0FBQztBQUMzQyxRQUFBLG9CQUFvQixHQUFHLFFBQVEsQ0FBQztBQUNoQyxRQUFBLG1CQUFtQixHQUFHLFFBQVEsQ0FBQztBQUUvQixRQUFBLGdCQUFnQixHQUFHLElBQUkscUJBQWMsQ0FBUyxrQkFBa0IsQ0FBQyxDQUFDO0FBRWxFLFFBQUEsd0JBQXdCLEdBQWtDLENBQ3JFLENBQUMsRUFDRCxDQUFDLEVBQ08sRUFBRTtJQUNWLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUU7UUFDWixNQUFNLElBQUksS0FBSyxDQUFDLDJDQUEyQyxDQUFDLENBQUM7S0FDOUQ7SUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7QUFDN0IsQ0FBQyxDQUFDO0FBRVcsUUFBQSxzQkFBc0IsR0FBb0MsQ0FDckUsS0FBdUIsRUFDdkIsRUFBRTtJQUNGLE9BQU8sS0FBSyxDQUFDLEVBQUUsQ0FBQztBQUNsQixDQUFDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3Rpb25Ub2tlbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tcGFyZXJTdHIsIElkU2VsZWN0b3JTdHIgfSBmcm9tICdAbmdyeC9lbnRpdHkvc3JjL21vZGVscyc7XG5pbXBvcnQgeyBReEZpcmViYXNlT2JqZWN0IH0gZnJvbSAnLi9maXJlYmFzZS1vYmplY3QubW9kZWwnO1xuXG5leHBvcnQgY29uc3QgRkVBVFVSRV9FVkVOVFNfRU5USVRZX1NUQVRFID0gJ2V2ZW50X2xpc3QnO1xuZXhwb3J0IGNvbnN0IEZFQVRVUkVfRVZFTlRTX1NMSUNFID0gJ2V2ZW50cyc7XG5leHBvcnQgY29uc3QgRkVBVFVSRV9RVUVVRV9TTElDRSA9ICdxdWV1ZXMnO1xuXG5leHBvcnQgY29uc3QgQ09MTEVDVElPTl9TTElDRSA9IG5ldyBJbmplY3Rpb25Ub2tlbjxzdHJpbmc+KCdDb2xsZWN0aW9uIFNsaWNlJyk7XG5cbmV4cG9ydCBjb25zdCBjb21wYXJlRW50aXR5SWRzQXNTdHJpbmc6IENvbXBhcmVyU3RyPFF4RmlyZWJhc2VPYmplY3Q+ID0gKFxuICBhLFxuICBiXG4pOiBzdHJpbmcgPT4ge1xuICBpZiAoIWEgJiYgIWIpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ0F0IGxlYXN0IDEgYXR0cmlidXRlIE1VU1QgaGF2ZSBhIHZhbGlkIElEJyk7XG4gIH1cbiAgcmV0dXJuIGEgPiBiID8gYS5pZCA6IGIuaWQ7XG59O1xuXG5leHBvcnQgY29uc3Qgc2VsZWN0RW50aXR5SWRBc1N0cmluZzogSWRTZWxlY3RvclN0cjxReEZpcmViYXNlT2JqZWN0PiA9IChcbiAgbW9kZWw6IFF4RmlyZWJhc2VPYmplY3RcbikgPT4ge1xuICByZXR1cm4gbW9kZWwuaWQ7XG59O1xuIl19