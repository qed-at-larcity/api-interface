"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
const dismissable_model_1 = require("./dismissable.model");
var DocumentResponse;
(function (DocumentResponse) {
    DocumentResponse["ACCEPT"] = "ok";
    DocumentResponse["REJECT"] = "reject";
})(DocumentResponse = exports.DocumentResponse || (exports.DocumentResponse = {}));
class InteractiveDocument extends dismissable_model_1.Dismissable {
    constructor() {
        super(...arguments);
        /**
         * All interactive documents are dismissable by default
         */
        this.dismissable = true;
        /**
         * This event will be emitted when the user responds
         */
        this.userResponse = new core_1.EventEmitter();
    }
}
InteractiveDocument.propDecorators = {
    userResponse: [{ type: core_1.Output }]
};
exports.InteractiveDocument = InteractiveDocument;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJhY3RpdmUtZG9jLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHF1dXAtbXZwLW54L3NoYXJlZC1hcGktaW50ZXJmYWNlLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9pbnRlcmFjdGl2ZS1kb2MubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSx3Q0FBcUQ7QUFDckQsMkRBQWtEO0FBRWxELElBQVksZ0JBR1g7QUFIRCxXQUFZLGdCQUFnQjtJQUMxQixpQ0FBYSxDQUFBO0lBQ2IscUNBQWlCLENBQUE7QUFDbkIsQ0FBQyxFQUhXLGdCQUFnQixHQUFoQix3QkFBZ0IsS0FBaEIsd0JBQWdCLFFBRzNCO0FBRUQsTUFBc0IsbUJBQW9CLFNBQVEsK0JBRWpEO0lBRkQ7O1FBR0U7O1dBRUc7UUFDSCxnQkFBVyxHQUFHLElBQUksQ0FBQztRQUVuQjs7V0FFRztRQUVILGlCQUFZLEdBQW1DLElBQUksbUJBQVksRUFBRSxDQUFDO0lBQ3BFLENBQUM7OzsyQkFGRSxhQUFNOztBQVhULGtEQWFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IERpc21pc3NhYmxlIH0gZnJvbSAnLi9kaXNtaXNzYWJsZS5tb2RlbCc7XG5cbmV4cG9ydCBlbnVtIERvY3VtZW50UmVzcG9uc2Uge1xuICBBQ0NFUFQgPSAnb2snLFxuICBSRUpFQ1QgPSAncmVqZWN0J1xufVxuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgSW50ZXJhY3RpdmVEb2N1bWVudCBleHRlbmRzIERpc21pc3NhYmxlPFxuICBJbnRlcmFjdGl2ZURvY3VtZW50XG4+IHtcbiAgLyoqXG4gICAqIEFsbCBpbnRlcmFjdGl2ZSBkb2N1bWVudHMgYXJlIGRpc21pc3NhYmxlIGJ5IGRlZmF1bHRcbiAgICovXG4gIGRpc21pc3NhYmxlID0gdHJ1ZTtcblxuICAvKipcbiAgICogVGhpcyBldmVudCB3aWxsIGJlIGVtaXR0ZWQgd2hlbiB0aGUgdXNlciByZXNwb25kc1xuICAgKi9cbiAgQE91dHB1dCgpXG4gIHVzZXJSZXNwb25zZTogRXZlbnRFbWl0dGVyPERvY3VtZW50UmVzcG9uc2U+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xufVxuIl19