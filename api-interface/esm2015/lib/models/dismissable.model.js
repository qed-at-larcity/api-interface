"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
class Dismissable {
    constructor() {
        /**
         * Set dismissable property to true by default
         */
        this.dismissable = true;
        /**
         * Emit instance of component being dismissed
         */
        this.dismiss = new core_1.EventEmitter();
    }
}
exports.Dismissable = Dismissable;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlzbWlzc2FibGUubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcXV1cC1tdnAtbngvc2hhcmVkLWFwaS1pbnRlcmZhY2UvIiwic291cmNlcyI6WyJsaWIvbW9kZWxzL2Rpc21pc3NhYmxlLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsd0NBQTZDO0FBRTdDLE1BQXNCLFdBQVc7SUFBakM7UUFDRTs7V0FFRztRQUNILGdCQUFXLEdBQUcsSUFBSSxDQUFDO1FBRW5COztXQUVHO1FBQ0gsWUFBTyxHQUFHLElBQUksbUJBQVksRUFBSyxDQUFDO0lBQ2xDLENBQUM7Q0FBQTtBQVZELGtDQVVDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBEaXNtaXNzYWJsZTxUPiB7XG4gIC8qKlxuICAgKiBTZXQgZGlzbWlzc2FibGUgcHJvcGVydHkgdG8gdHJ1ZSBieSBkZWZhdWx0XG4gICAqL1xuICBkaXNtaXNzYWJsZSA9IHRydWU7XG5cbiAgLyoqXG4gICAqIEVtaXQgaW5zdGFuY2Ugb2YgY29tcG9uZW50IGJlaW5nIGRpc21pc3NlZFxuICAgKi9cbiAgZGlzbWlzcyA9IG5ldyBFdmVudEVtaXR0ZXI8VD4oKTtcbn1cbiJdfQ==