(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('@angular/core'), require('@ngrx/store'), require('@ngrx/entity')) :
	typeof define === 'function' && define.amd ? define('@quup-mvp-nx/shared-api-interface', ['@angular/core', '@ngrx/store', '@ngrx/entity'], factory) :
	(global['quup-mvp-nx'] = global['quup-mvp-nx'] || {}, global['quup-mvp-nx']['shared-api-interface'] = factory(global.ng.core,global.store,global.entity));
}(this, (function (core,store,entity) { 'use strict';

	core = core && core.hasOwnProperty('default') ? core['default'] : core;
	store = store && store.hasOwnProperty('default') ? store['default'] : store;
	entity = entity && entity.hasOwnProperty('default') ? entity['default'] : entity;

	function unwrapExports(x) {
	    return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
	}
	function createCommonjsModule(fn, module) {
	    return module = { exports: {} }, fn(module, module.exports), module.exports;
	}

	/*! *****************************************************************************
	Copyright (c) Microsoft Corporation. All rights reserved.
	Licensed under the Apache License, Version 2.0 (the "License"); you may not use
	this file except in compliance with the License. You may obtain a copy of the
	License at http://www.apache.org/licenses/LICENSE-2.0

	THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
	KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
	WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
	MERCHANTABLITY OR NON-INFRINGEMENT.

	See the Apache Version 2.0 License for specific language governing permissions
	and limitations under the License.
	***************************************************************************** */
	/* global Reflect, Promise */
	var extendStatics = function (d, b) {
	    extendStatics = Object.setPrototypeOf ||
	        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
	        function (d, b) { for (var p in b)
	            if (b.hasOwnProperty(p))
	                d[p] = b[p]; };
	    return extendStatics(d, b);
	};
	function __extends(d, b) {
	    extendStatics(d, b);
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	}
	var __assign = function () {
	    __assign = Object.assign || function __assign(t) {
	        for (var s, i = 1, n = arguments.length; i < n; i++) {
	            s = arguments[i];
	            for (var p in s)
	                if (Object.prototype.hasOwnProperty.call(s, p))
	                    t[p] = s[p];
	        }
	        return t;
	    };
	    return __assign.apply(this, arguments);
	};
	function __rest(s, e) {
	    var t = {};
	    for (var p in s)
	        if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
	            t[p] = s[p];
	    if (s != null && typeof Object.getOwnPropertySymbols === "function")
	        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
	            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
	                t[p[i]] = s[p[i]];
	        }
	    return t;
	}
	function __decorate(decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function")
	        r = Reflect.decorate(decorators, target, key, desc);
	    else
	        for (var i = decorators.length - 1; i >= 0; i--)
	            if (d = decorators[i])
	                r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	}
	function __param(paramIndex, decorator) {
	    return function (target, key) { decorator(target, key, paramIndex); };
	}
	function __metadata(metadataKey, metadataValue) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function")
	        return Reflect.metadata(metadataKey, metadataValue);
	}
	function __awaiter(thisArg, _arguments, P, generator) {
	    return new (P || (P = Promise))(function (resolve, reject) {
	        function fulfilled(value) { try {
	            step(generator.next(value));
	        }
	        catch (e) {
	            reject(e);
	        } }
	        function rejected(value) { try {
	            step(generator["throw"](value));
	        }
	        catch (e) {
	            reject(e);
	        } }
	        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
	        step((generator = generator.apply(thisArg, _arguments || [])).next());
	    });
	}
	function __generator(thisArg, body) {
	    var _ = { label: 0, sent: function () { if (t[0] & 1)
	            throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
	    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function () { return this; }), g;
	    function verb(n) { return function (v) { return step([n, v]); }; }
	    function step(op) {
	        if (f)
	            throw new TypeError("Generator is already executing.");
	        while (_)
	            try {
	                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done)
	                    return t;
	                if (y = 0, t)
	                    op = [op[0] & 2, t.value];
	                switch (op[0]) {
	                    case 0:
	                    case 1:
	                        t = op;
	                        break;
	                    case 4:
	                        _.label++;
	                        return { value: op[1], done: false };
	                    case 5:
	                        _.label++;
	                        y = op[1];
	                        op = [0];
	                        continue;
	                    case 7:
	                        op = _.ops.pop();
	                        _.trys.pop();
	                        continue;
	                    default:
	                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
	                            _ = 0;
	                            continue;
	                        }
	                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
	                            _.label = op[1];
	                            break;
	                        }
	                        if (op[0] === 6 && _.label < t[1]) {
	                            _.label = t[1];
	                            t = op;
	                            break;
	                        }
	                        if (t && _.label < t[2]) {
	                            _.label = t[2];
	                            _.ops.push(op);
	                            break;
	                        }
	                        if (t[2])
	                            _.ops.pop();
	                        _.trys.pop();
	                        continue;
	                }
	                op = body.call(thisArg, _);
	            }
	            catch (e) {
	                op = [6, e];
	                y = 0;
	            }
	            finally {
	                f = t = 0;
	            }
	        if (op[0] & 5)
	            throw op[1];
	        return { value: op[0] ? op[1] : void 0, done: true };
	    }
	}
	function __exportStar(m, exports) {
	    for (var p in m)
	        if (!exports.hasOwnProperty(p))
	            exports[p] = m[p];
	}
	function __values(o) {
	    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
	    if (m)
	        return m.call(o);
	    return {
	        next: function () {
	            if (o && i >= o.length)
	                o = void 0;
	            return { value: o && o[i++], done: !o };
	        }
	    };
	}
	function __read(o, n) {
	    var m = typeof Symbol === "function" && o[Symbol.iterator];
	    if (!m)
	        return o;
	    var i = m.call(o), r, ar = [], e;
	    try {
	        while ((n === void 0 || n-- > 0) && !(r = i.next()).done)
	            ar.push(r.value);
	    }
	    catch (error) {
	        e = { error: error };
	    }
	    finally {
	        try {
	            if (r && !r.done && (m = i["return"]))
	                m.call(i);
	        }
	        finally {
	            if (e)
	                throw e.error;
	        }
	    }
	    return ar;
	}
	function __spread() {
	    for (var ar = [], i = 0; i < arguments.length; i++)
	        ar = ar.concat(__read(arguments[i]));
	    return ar;
	}
	function __spreadArrays() {
	    for (var s = 0, i = 0, il = arguments.length; i < il; i++)
	        s += arguments[i].length;
	    for (var r = Array(s), k = 0, i = 0; i < il; i++)
	        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
	            r[k] = a[j];
	    return r;
	}
	function __await(v) {
	    return this instanceof __await ? (this.v = v, this) : new __await(v);
	}
	function __asyncGenerator(thisArg, _arguments, generator) {
	    if (!Symbol.asyncIterator)
	        throw new TypeError("Symbol.asyncIterator is not defined.");
	    var g = generator.apply(thisArg, _arguments || []), i, q = [];
	    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
	    function verb(n) { if (g[n])
	        i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
	    function resume(n, v) { try {
	        step(g[n](v));
	    }
	    catch (e) {
	        settle(q[0][3], e);
	    } }
	    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
	    function fulfill(value) { resume("next", value); }
	    function reject(value) { resume("throw", value); }
	    function settle(f, v) { if (f(v), q.shift(), q.length)
	        resume(q[0][0], q[0][1]); }
	}
	function __asyncDelegator(o) {
	    var i, p;
	    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
	    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
	}
	function __asyncValues(o) {
	    if (!Symbol.asyncIterator)
	        throw new TypeError("Symbol.asyncIterator is not defined.");
	    var m = o[Symbol.asyncIterator], i;
	    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
	    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
	    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function (v) { resolve({ value: v, done: d }); }, reject); }
	}
	function __makeTemplateObject(cooked, raw) {
	    if (Object.defineProperty) {
	        Object.defineProperty(cooked, "raw", { value: raw });
	    }
	    else {
	        cooked.raw = raw;
	    }
	    return cooked;
	}
	function __importStar(mod) {
	    if (mod && mod.__esModule)
	        return mod;
	    var result = {};
	    if (mod != null)
	        for (var k in mod)
	            if (Object.hasOwnProperty.call(mod, k))
	                result[k] = mod[k];
	    result.default = mod;
	    return result;
	}
	function __importDefault(mod) {
	    return (mod && mod.__esModule) ? mod : { default: mod };
	}

	var tslib_es6 = /*#__PURE__*/Object.freeze({
		__extends: __extends,
		get __assign () { return __assign; },
		__rest: __rest,
		__decorate: __decorate,
		__param: __param,
		__metadata: __metadata,
		__awaiter: __awaiter,
		__generator: __generator,
		__exportStar: __exportStar,
		__values: __values,
		__read: __read,
		__spread: __spread,
		__spreadArrays: __spreadArrays,
		__await: __await,
		__asyncGenerator: __asyncGenerator,
		__asyncDelegator: __asyncDelegator,
		__asyncValues: __asyncValues,
		__makeTemplateObject: __makeTemplateObject,
		__importStar: __importStar,
		__importDefault: __importDefault
	});

	var app_routes = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    var AppRoute;
	    (function (AppRoute) {
	        AppRoute["AUTH"] = "auth";
	        AppRoute["ACCOUNT"] = "account";
	        AppRoute["PRODUCT"] = "product";
	        AppRoute["EVENTS"] = "events";
	        AppRoute["WIZARD_EVENT"] = "wizard/event";
	    })(AppRoute = exports.AppRoute || (exports.AppRoute = {}));
	    
	});
	unwrapExports(app_routes);
	var app_routes_1 = app_routes.AppRoute;

	var features_routes = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    exports.appFeatureRoutes = [
	        {
	            path: app_routes.AppRoute.PRODUCT,
	            loadChildren: '@quup-mvp-nx/feature-product-mvp#FeatureProductMvpModule'
	        },
	        {
	            path: app_routes.AppRoute.AUTH,
	            loadChildren: '@quup-mvp-nx/shared/feature-auth#FeatureAuthModule'
	        },
	        /**
	         * @TODO design and code the organization context for all routes below
	         * this section
	         */
	        {
	            path: app_routes.AppRoute.ACCOUNT,
	            loadChildren: '@quup-mvp-nx/shared/feature-account#FeatureAccountModule'
	        },
	        {
	            path: app_routes.AppRoute.EVENTS,
	            loadChildren: '@quup-mvp-nx/feature-events#FeatureEventsModule'
	        }
	    ];
	    
	});
	unwrapExports(features_routes);
	var features_routes_1 = features_routes.appFeatureRoutes;

	var wizard_routes = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    /**
	     * MUST Be implemented at the root routing level of the app to be able
	     * to target the referenced app outlet
	     */
	    exports.WizardRouteEvent = {
	        path: app_routes.AppRoute.WIZARD_EVENT,
	        outlet: 'modal',
	        loadChildren: '@quup-mvp-nx/feature-events#FeatureEventsModule'
	    };
	    
	});
	unwrapExports(wizard_routes);
	var wizard_routes_1 = wizard_routes.WizardRouteEvent;

	var routes = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    tslib_es6.__exportStar(app_routes, exports);
	    tslib_es6.__exportStar(features_routes, exports);
	    tslib_es6.__exportStar(wizard_routes, exports);
	    
	});
	unwrapExports(routes);

	var cmsObject_model = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    var CmsObjectStatus;
	    (function (CmsObjectStatus) {
	        CmsObjectStatus["PUBLISHED"] = "published";
	        CmsObjectStatus["DRAFT"] = "draft";
	    })(CmsObjectStatus = exports.CmsObjectStatus || (exports.CmsObjectStatus = {}));
	    
	});
	unwrapExports(cmsObject_model);
	var cmsObject_model_1 = cmsObject_model.CmsObjectStatus;

	var firebaseObject_model = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    exports.DRAFT_DOC_INDEX = 'zDraftDocument';
	    
	});
	unwrapExports(firebaseObject_model);
	var firebaseObject_model_1 = firebaseObject_model.DRAFT_DOC_INDEX;

	var firebaseAuthError_model = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    var QxFirebaseAuthErrorCodes;
	    (function (QxFirebaseAuthErrorCodes) {
	        QxFirebaseAuthErrorCodes["EMAIL_ALREADY_EXISTS"] = "auth/email-already-exists";
	    })(QxFirebaseAuthErrorCodes = exports.QxFirebaseAuthErrorCodes || (exports.QxFirebaseAuthErrorCodes = {}));
	    
	});
	unwrapExports(firebaseAuthError_model);
	var firebaseAuthError_model_1 = firebaseAuthError_model.QxFirebaseAuthErrorCodes;

	var timeRecord_model = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    // import * as moment from 'moment-timezone';
	    var TimeRecord = /** @class */ (function () {
	        function TimeRecord(utc, unix) {
	            if (utc === void 0) {
	                utc = new Date().toISOString();
	            }
	            if (unix === void 0) {
	                unix = new Date().getTime();
	            }
	            this.utc = utc;
	            this.unix = unix;
	        }
	        // utc: string;
	        // unix: number;
	        TimeRecord.NOW = function (timezone) {
	            if (timezone === void 0) {
	                timezone = 'America/New_York';
	            }
	            // const now = moment(new Date()).tz(timezone);
	            return TimeRecord.sanitize(new TimeRecord());
	        };
	        TimeRecord.sanitize = function (_a) {
	            var utc = _a.utc, unix = _a.unix;
	            return { utc: utc, unix: unix };
	        };
	        return TimeRecord;
	    }());
	    exports.TimeRecord = TimeRecord;
	    exports.DURATION_INTERVALS = [
	        { label: 'minutes', value: 1 },
	        { label: 'hours', value: 60 },
	        { label: 'days', value: 24 * 60 },
	        { label: 'weeks', value: 7 * 24 * 60 },
	        { label: 'months', value: 30 * 7 * 24 * 60 }
	    ];
	    
	});
	unwrapExports(timeRecord_model);
	var timeRecord_model_1 = timeRecord_model.TimeRecord;
	var timeRecord_model_2 = timeRecord_model.DURATION_INTERVALS;

	var userData_model = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    exports.COLL_PATH_USER_REF = 'userInfo';
	    exports.COLL_PATH_USERDATA_REF = 'userData';
	    
	});
	unwrapExports(userData_model);
	var userData_model_1 = userData_model.COLL_PATH_USER_REF;
	var userData_model_2 = userData_model.COLL_PATH_USERDATA_REF;

	var environment_model = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    exports.APP_CONFIG_TOKEN = new core.InjectionToken('App config');
	    
	});
	unwrapExports(environment_model);
	var environment_model_1 = environment_model.APP_CONFIG_TOKEN;

	var dismissable_model = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    var Dismissable = /** @class */ (function () {
	        function Dismissable() {
	            /**
	             * Set dismissable property to true by default
	             */
	            this.dismissable = true;
	            /**
	             * Emit instance of component being dismissed
	             */
	            this.dismiss = new core.EventEmitter();
	        }
	        return Dismissable;
	    }());
	    exports.Dismissable = Dismissable;
	    
	});
	unwrapExports(dismissable_model);
	var dismissable_model_1 = dismissable_model.Dismissable;

	var interactiveDoc_model = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    var DocumentResponse;
	    (function (DocumentResponse) {
	        DocumentResponse["ACCEPT"] = "ok";
	        DocumentResponse["REJECT"] = "reject";
	    })(DocumentResponse = exports.DocumentResponse || (exports.DocumentResponse = {}));
	    var InteractiveDocument = /** @class */ (function (_super) {
	        tslib_es6.__extends(InteractiveDocument, _super);
	        function InteractiveDocument() {
	            var _this = _super !== null && _super.apply(this, arguments) || this;
	            /**
	             * All interactive documents are dismissable by default
	             */
	            _this.dismissable = true;
	            /**
	             * This event will be emitted when the user responds
	             */
	            _this.userResponse = new core.EventEmitter();
	            return _this;
	        }
	        InteractiveDocument.propDecorators = {
	            userResponse: [{ type: core.Output }]
	        };
	        return InteractiveDocument;
	    }(dismissable_model.Dismissable));
	    exports.InteractiveDocument = InteractiveDocument;
	    
	});
	unwrapExports(interactiveDoc_model);
	var interactiveDoc_model_1 = interactiveDoc_model.DocumentResponse;
	var interactiveDoc_model_2 = interactiveDoc_model.InteractiveDocument;

	var ui_model = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    var UIAlertType;
	    (function (UIAlertType) {
	        UIAlertType["WARNING"] = "alert-warning";
	        UIAlertType["SUCCESS"] = "alert-success";
	        UIAlertType["ERROR"] = "alert-danger";
	        UIAlertType["INFO"] = "alert-info";
	        UIAlertType["HINT"] = "alert-light";
	    })(UIAlertType = exports.UIAlertType || (exports.UIAlertType = {}));
	    var FormatTime;
	    (function (FormatTime) {
	        FormatTime["LONG"] = "dddd, MMMM Do YYYY, h:mm:ss a z";
	        FormatTime["SHORT"] = "ddd, MMM Do YYYY, h:mm a";
	    })(FormatTime = exports.FormatTime || (exports.FormatTime = {}));
	    var TimeZones;
	    (function (TimeZones) {
	        TimeZones["EST"] = "America/New_York";
	        TimeZones["WAT"] = "Africa/Lagos"; // +01:00
	    })(TimeZones = exports.TimeZones || (exports.TimeZones = {}));
	    exports.DEFAULT_TIME_ZONE = new core.InjectionToken('Default TimeZone');
	    exports.FORMAT_TIME_LONG = 'dddd, MMMM Do YYYY, h:mm:ss a';
	    exports.FORMAT_TIME_SHORT = 'ddd, MMM Do YYYY, h:mm a';
	    exports.UI_TOAST_ALERT_TYPE = UIAlertType.INFO;
	    exports.UI_TOAST_DISMISS_TIMEOUT = new core.InjectionToken('UI Toast Dismiss Timeout');
	    exports.MODAL_OVERLAY_REF = new core.InjectionToken('Modal OverlayRef');
	    
	});
	unwrapExports(ui_model);
	var ui_model_1 = ui_model.UIAlertType;
	var ui_model_2 = ui_model.FormatTime;
	var ui_model_3 = ui_model.TimeZones;
	var ui_model_4 = ui_model.DEFAULT_TIME_ZONE;
	var ui_model_5 = ui_model.FORMAT_TIME_LONG;
	var ui_model_6 = ui_model.FORMAT_TIME_SHORT;
	var ui_model_7 = ui_model.UI_TOAST_ALERT_TYPE;
	var ui_model_8 = ui_model.UI_TOAST_DISMISS_TIMEOUT;
	var ui_model_9 = ui_model.MODAL_OVERLAY_REF;

	var stateFeatureAccount_model = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    exports.FEATURE_ACCOUNT_SLICE = 'account';
	    exports.initialFeatureAccountState = {
	        records: []
	    };
	    
	});
	unwrapExports(stateFeatureAccount_model);
	var stateFeatureAccount_model_1 = stateFeatureAccount_model.FEATURE_ACCOUNT_SLICE;
	var stateFeatureAccount_model_2 = stateFeatureAccount_model.initialFeatureAccountState;

	var stateFeatureAuth_model = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    exports.initialFeatureAuthState = {
	        agreementRecords: []
	    };
	    exports.AUTH_FEATURE_KEY = 'auth';
	    
	});
	unwrapExports(stateFeatureAuth_model);
	var stateFeatureAuth_model_1 = stateFeatureAuth_model.initialFeatureAuthState;
	var stateFeatureAuth_model_2 = stateFeatureAuth_model.AUTH_FEATURE_KEY;

	var stateOrg_model = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    exports.initialOrgState = {
	        ids: [],
	        entities: {}
	    };
	    exports.ORG_SLICE = 'organizations';
	    
	});
	unwrapExports(stateOrg_model);
	var stateOrg_model_1 = stateOrg_model.initialOrgState;
	var stateOrg_model_2 = stateOrg_model.ORG_SLICE;

	var stateSession_model = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    exports.initialSessionState = {
	        startTime: new Date().getTime()
	    };
	    
	});
	unwrapExports(stateSession_model);
	var stateSession_model_1 = stateSession_model.initialSessionState;

	var stateTasks_model = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    exports.TASKS_SLICE = 'tasks';
	    exports.initialTaskState = {
	        taskCounter: 0,
	        ids: [],
	        entities: {}
	    };
	    
	});
	unwrapExports(stateTasks_model);
	var stateTasks_model_1 = stateTasks_model.TASKS_SLICE;
	var stateTasks_model_2 = stateTasks_model.initialTaskState;

	var stateApp_model = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    var _a;
	    exports.initialAppState = (_a = {
	        auth: stateFeatureAuth_model.initialFeatureAuthState,
	        session: stateSession_model.initialSessionState
	    },
	        _a[stateTasks_model.TASKS_SLICE] = stateTasks_model.initialTaskState,
	        _a[stateFeatureAccount_model.FEATURE_ACCOUNT_SLICE] = stateFeatureAccount_model.initialFeatureAccountState,
	        _a[stateOrg_model.ORG_SLICE] = stateOrg_model.initialOrgState,
	        _a);
	    
	});
	unwrapExports(stateApp_model);
	var stateApp_model_1 = stateApp_model.initialAppState;

	var stateFeature_model = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    exports.FEATURE_EVENTS_ENTITY_STATE = 'event_list';
	    exports.FEATURE_EVENTS_SLICE = 'events';
	    exports.FEATURE_QUEUE_SLICE = 'queues';
	    exports.COLLECTION_SLICE = new core.InjectionToken('Collection Slice');
	    exports.compareEntityIdsAsString = function (a, b) {
	        if (!a && !b) {
	            throw new Error('At least 1 attribute MUST have a valid ID');
	        }
	        return a > b ? a.id : b.id;
	    };
	    exports.selectEntityIdAsString = function (model) {
	        return model.id;
	    };
	    
	});
	unwrapExports(stateFeature_model);
	var stateFeature_model_1 = stateFeature_model.FEATURE_EVENTS_ENTITY_STATE;
	var stateFeature_model_2 = stateFeature_model.FEATURE_EVENTS_SLICE;
	var stateFeature_model_3 = stateFeature_model.FEATURE_QUEUE_SLICE;
	var stateFeature_model_4 = stateFeature_model.COLLECTION_SLICE;
	var stateFeature_model_5 = stateFeature_model.compareEntityIdsAsString;
	var stateFeature_model_6 = stateFeature_model.selectEntityIdAsString;

	var stateCourses_model = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    exports.initialCourseAliasState = {
	        ids: [],
	        entities: {},
	        selectedCourseAliasId: null
	    };
	    exports.COURSE_ALIAS_SLICE = 'courses';
	    
	});
	unwrapExports(stateCourses_model);
	var stateCourses_model_1 = stateCourses_model.initialCourseAliasState;
	var stateCourses_model_2 = stateCourses_model.COURSE_ALIAS_SLICE;

	var ngrx_model = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    var ErrorActionProp = /** @class */ (function () {
	        function ErrorActionProp(error, length) {
	            if (length === void 0) {
	                length = 0;
	            }
	            this.error = error;
	            this.length = length;
	        }
	        return ErrorActionProp;
	    }());
	    exports.ErrorActionProp = ErrorActionProp;
	    function ErrorAction(error, length) {
	        if (length === void 0) {
	            length = 0;
	        }
	        return new ErrorActionProp(error, length);
	    }
	    exports.ErrorAction = ErrorAction;
	    
	});
	unwrapExports(ngrx_model);
	var ngrx_model_1 = ngrx_model.ErrorActionProp;
	var ngrx_model_2 = ngrx_model.ErrorAction;

	var models = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    /**
	     * Shared (Backend + Frontend) models
	     */
	    tslib_es6.__exportStar(cmsObject_model, exports);
	    tslib_es6.__exportStar(firebaseObject_model, exports);
	    tslib_es6.__exportStar(firebaseAuthError_model, exports);
	    tslib_es6.__exportStar(timeRecord_model, exports);
	    tslib_es6.__exportStar(userData_model, exports);
	    /**
	     * Frontend models
	     */
	    tslib_es6.__exportStar(environment_model, exports);
	    tslib_es6.__exportStar(dismissable_model, exports);
	    tslib_es6.__exportStar(interactiveDoc_model, exports);
	    tslib_es6.__exportStar(ui_model, exports);
	    tslib_es6.__exportStar(stateApp_model, exports);
	    tslib_es6.__exportStar(stateFeature_model, exports);
	    tslib_es6.__exportStar(stateFeatureAuth_model, exports);
	    tslib_es6.__exportStar(stateFeatureAccount_model, exports);
	    tslib_es6.__exportStar(stateCourses_model, exports);
	    tslib_es6.__exportStar(stateTasks_model, exports);
	    tslib_es6.__exportStar(stateOrg_model, exports);
	    // export * from './state-queue.model';
	    tslib_es6.__exportStar(ngrx_model, exports);
	    
	});
	unwrapExports(models);

	var stateCourseAlias_entity = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    function sortCourseAliasById(e1, e2) {
	        return e1.id - e2.id;
	    }
	    exports.sortCourseAliasById = sortCourseAliasById;
	    function sortCourseAliasIds(id1, id2) {
	        return id1 - id2;
	    }
	    exports.sortCourseAliasIds = sortCourseAliasIds;
	    function reduceCourseAliasArrayToMap(map, courseAlias) {
	        map[courseAlias.id] = courseAlias;
	        return map;
	    }
	    exports.reduceCourseAliasArrayToMap = reduceCourseAliasArrayToMap;
	    exports.selectCourseAliasEntityId = function (courseAlias) {
	        return courseAlias.id;
	    };
	    exports.courseAliasEntityAdapter = entity.createEntityAdapter({
	        sortComparer: sortCourseAliasById,
	        selectId: exports.selectCourseAliasEntityId
	    });
	    
	});
	unwrapExports(stateCourseAlias_entity);
	var stateCourseAlias_entity_1 = stateCourseAlias_entity.sortCourseAliasById;
	var stateCourseAlias_entity_2 = stateCourseAlias_entity.sortCourseAliasIds;
	var stateCourseAlias_entity_3 = stateCourseAlias_entity.reduceCourseAliasArrayToMap;
	var stateCourseAlias_entity_4 = stateCourseAlias_entity.selectCourseAliasEntityId;
	var stateCourseAlias_entity_5 = stateCourseAlias_entity.courseAliasEntityAdapter;

	var stateInteractiveEvent_entity = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    exports.compareEventByTimeCreated = function (a, b) {
	        if (!a.time_created) {
	            return !b.time_created ? 0 : b.time_created.unix;
	        }
	        else if (!b.time_created) {
	            return !a.time_created ? 0 : a.time_created.unix;
	        }
	        return a.time_created.unix > b.time_created.unix
	            ? b.time_created.unix
	            : a.time_created.unix;
	    };
	    exports.interactiveEventEntityAdapter = entity.createEntityAdapter({
	        // sortComparer: compareEntityIdsAsString,
	        sortComparer: exports.compareEventByTimeCreated,
	        selectId: models.selectEntityIdAsString
	    });
	    exports.selectInteractiveEventState = store.createFeatureSelector(models.FEATURE_EVENTS_SLICE);
	    var _a = exports.interactiveEventEntityAdapter.getSelectors(), selectAll = _a.selectAll, selectEntities = _a.selectEntities, selectIds = _a.selectIds;
	    exports.selectAllInteractiveEvents = store.createSelector(exports.selectInteractiveEventState, function (state) { return selectAll(state); });
	    exports.selectInteractiveEventEntities = store.createSelector(exports.selectInteractiveEventState, function (state) { return selectEntities(state); });
	    exports.selectInteractiveEventIds = store.createSelector(exports.selectInteractiveEventState, function (state) { return selectIds(state); });
	    
	});
	unwrapExports(stateInteractiveEvent_entity);
	var stateInteractiveEvent_entity_1 = stateInteractiveEvent_entity.compareEventByTimeCreated;
	var stateInteractiveEvent_entity_2 = stateInteractiveEvent_entity.interactiveEventEntityAdapter;
	var stateInteractiveEvent_entity_3 = stateInteractiveEvent_entity.selectInteractiveEventState;
	var stateInteractiveEvent_entity_4 = stateInteractiveEvent_entity.selectAllInteractiveEvents;
	var stateInteractiveEvent_entity_5 = stateInteractiveEvent_entity.selectInteractiveEventEntities;
	var stateInteractiveEvent_entity_6 = stateInteractiveEvent_entity.selectInteractiveEventIds;

	var stateOrg_entity = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    exports.orgEntityAdapter = entity.createEntityAdapter({
	        selectId: function (org) { return org.id; },
	        sortComparer: function (org1, org2) { return (org1.id > org2.id ? org1.id : org2.id); }
	    });
	    
	});
	unwrapExports(stateOrg_entity);
	var stateOrg_entity_1 = stateOrg_entity.orgEntityAdapter;

	var stateTasks_entity = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    var sortComparer = function (task1, task2) {
	        return task1.id > task2.id ? task1.id : task2.id;
	    };
	    var ɵ0 = sortComparer;
	    exports.ɵ0 = ɵ0;
	    var selectId = function (task) { return task.id; };
	    var ɵ1 = selectId;
	    exports.ɵ1 = ɵ1;
	    exports.taskEntityAdapter = entity.createEntityAdapter({
	        sortComparer: sortComparer,
	        selectId: selectId
	    });
	    
	});
	unwrapExports(stateTasks_entity);
	var stateTasks_entity_1 = stateTasks_entity.taskEntityAdapter;

	var stateQueue_entity = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    exports.initialQueueEntityState = {
	        ids: [],
	        entities: {}
	    };
	    exports.interactiveQueueEntityAdapter = entity.createEntityAdapter({
	        sortComparer: models.compareEntityIdsAsString,
	        selectId: models.selectEntityIdAsString
	    });
	    
	});
	unwrapExports(stateQueue_entity);
	var stateQueue_entity_1 = stateQueue_entity.initialQueueEntityState;
	var stateQueue_entity_2 = stateQueue_entity.interactiveQueueEntityAdapter;

	var entities = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    tslib_es6.__exportStar(stateCourseAlias_entity, exports);
	    tslib_es6.__exportStar(stateInteractiveEvent_entity, exports);
	    tslib_es6.__exportStar(stateOrg_entity, exports);
	    tslib_es6.__exportStar(stateTasks_entity, exports);
	    tslib_es6.__exportStar(stateQueue_entity, exports);
	    
	});
	unwrapExports(entities);

	var esm5 = createCommonjsModule(function (module, exports) {
	    Object.defineProperty(exports, "__esModule", { value: true });
	    tslib_es6.__exportStar(routes, exports);
	    tslib_es6.__exportStar(models, exports);
	    tslib_es6.__exportStar(entities, exports);
	    
	});
	unwrapExports(esm5);

	var quupMvpNxSharedApiInterface = createCommonjsModule(function (module, exports) {
	    /**
	     * Generated bundle index. Do not edit.
	     */
	    Object.defineProperty(exports, "__esModule", { value: true });
	    tslib_es6.__exportStar(esm5, exports);
	    
	});
	var quupMvpNxSharedApiInterface$1 = unwrapExports(quupMvpNxSharedApiInterface);

	return quupMvpNxSharedApiInterface$1;

})));

//# sourceMappingURL=quup-mvp-nx-shared-api-interface.umd.js.map