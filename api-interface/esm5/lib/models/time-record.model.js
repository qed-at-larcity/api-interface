"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import * as moment from 'moment-timezone';
var TimeRecord = /** @class */ (function () {
    function TimeRecord(utc, unix) {
        if (utc === void 0) { utc = new Date().toISOString(); }
        if (unix === void 0) { unix = new Date().getTime(); }
        this.utc = utc;
        this.unix = unix;
    }
    // utc: string;
    // unix: number;
    TimeRecord.NOW = function (timezone) {
        if (timezone === void 0) { timezone = 'America/New_York'; }
        // const now = moment(new Date()).tz(timezone);
        return TimeRecord.sanitize(new TimeRecord());
    };
    TimeRecord.sanitize = function (_a) {
        var utc = _a.utc, unix = _a.unix;
        return { utc: utc, unix: unix };
    };
    return TimeRecord;
}());
exports.TimeRecord = TimeRecord;
exports.DURATION_INTERVALS = [
    { label: 'minutes', value: 1 },
    { label: 'hours', value: 60 },
    { label: 'days', value: 24 * 60 },
    { label: 'weeks', value: 7 * 24 * 60 },
    { label: 'months', value: 30 * 7 * 24 * 60 }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZS1yZWNvcmQubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcXV1cC1tdnAtbngvc2hhcmVkLWFwaS1pbnRlcmZhY2UvIiwic291cmNlcyI6WyJsaWIvbW9kZWxzL3RpbWUtcmVjb3JkLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQ0EsNkNBQTZDO0FBRTdDO0lBWUUsb0JBQ1MsR0FBc0MsRUFDdEMsSUFBbUM7UUFEbkMsb0JBQUEsRUFBQSxNQUFjLElBQUksSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFO1FBQ3RDLHFCQUFBLEVBQUEsT0FBZSxJQUFJLElBQUksRUFBRSxDQUFDLE9BQU8sRUFBRTtRQURuQyxRQUFHLEdBQUgsR0FBRyxDQUFtQztRQUN0QyxTQUFJLEdBQUosSUFBSSxDQUErQjtJQUN6QyxDQUFDO0lBZEosZUFBZTtJQUNmLGdCQUFnQjtJQUNULGNBQUcsR0FBVixVQUFXLFFBQTZCO1FBQTdCLHlCQUFBLEVBQUEsNkJBQTZCO1FBQ3RDLCtDQUErQztRQUMvQyxPQUFPLFVBQVUsQ0FBQyxRQUFRLENBQUMsSUFBSSxVQUFVLEVBQUUsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFFTSxtQkFBUSxHQUFmLFVBQWdCLEVBQXlCO1lBQXZCLFlBQUcsRUFBRSxjQUFJO1FBQ3pCLE9BQU8sRUFBRSxHQUFHLEtBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFDO0lBQ3ZCLENBQUM7SUFNSCxpQkFBQztBQUFELENBQUMsQUFoQkQsSUFnQkM7QUFoQlksZ0NBQVU7QUFrQlYsUUFBQSxrQkFBa0IsR0FBNkI7SUFDMUQsRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUU7SUFDOUIsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUU7SUFDN0IsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFO0lBQ2pDLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEVBQUU7SUFDdEMsRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxFQUFFLEdBQUcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEVBQUU7Q0FDN0MsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEdlbmVyaWNGb3JtSW5wdXRPcHRpb24gfSBmcm9tICcuL2lucHV0LW9wdGlvbnMubW9kZWwnO1xuLy8gaW1wb3J0ICogYXMgbW9tZW50IGZyb20gJ21vbWVudC10aW1lem9uZSc7XG5cbmV4cG9ydCBjbGFzcyBUaW1lUmVjb3JkIHtcbiAgLy8gdXRjOiBzdHJpbmc7XG4gIC8vIHVuaXg6IG51bWJlcjtcbiAgc3RhdGljIE5PVyh0aW1lem9uZSA9ICdBbWVyaWNhL05ld19Zb3JrJyk6IFRpbWVSZWNvcmQge1xuICAgIC8vIGNvbnN0IG5vdyA9IG1vbWVudChuZXcgRGF0ZSgpKS50eih0aW1lem9uZSk7XG4gICAgcmV0dXJuIFRpbWVSZWNvcmQuc2FuaXRpemUobmV3IFRpbWVSZWNvcmQoKSk7XG4gIH1cblxuICBzdGF0aWMgc2FuaXRpemUoeyB1dGMsIHVuaXggfTogVGltZVJlY29yZCk6IFRpbWVSZWNvcmQge1xuICAgIHJldHVybiB7IHV0YywgdW5peCB9O1xuICB9XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHVibGljIHV0Yzogc3RyaW5nID0gbmV3IERhdGUoKS50b0lTT1N0cmluZygpLFxuICAgIHB1YmxpYyB1bml4OiBudW1iZXIgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKVxuICApIHt9XG59XG5cbmV4cG9ydCBjb25zdCBEVVJBVElPTl9JTlRFUlZBTFM6IEdlbmVyaWNGb3JtSW5wdXRPcHRpb25bXSA9IFtcbiAgeyBsYWJlbDogJ21pbnV0ZXMnLCB2YWx1ZTogMSB9LFxuICB7IGxhYmVsOiAnaG91cnMnLCB2YWx1ZTogNjAgfSxcbiAgeyBsYWJlbDogJ2RheXMnLCB2YWx1ZTogMjQgKiA2MCB9LFxuICB7IGxhYmVsOiAnd2Vla3MnLCB2YWx1ZTogNyAqIDI0ICogNjAgfSxcbiAgeyBsYWJlbDogJ21vbnRocycsIHZhbHVlOiAzMCAqIDcgKiAyNCAqIDYwIH1cbl07XG4iXX0=