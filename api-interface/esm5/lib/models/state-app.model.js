"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _a;
var state_feature_account_model_1 = require("./state-feature-account.model");
var state_feature_auth_model_1 = require("./state-feature-auth.model");
var state_org_model_1 = require("./state-org.model");
var state_session_model_1 = require("./state-session.model");
var state_tasks_model_1 = require("./state-tasks.model");
exports.initialAppState = (_a = {
        auth: state_feature_auth_model_1.initialFeatureAuthState,
        session: state_session_model_1.initialSessionState
    },
    _a[state_tasks_model_1.TASKS_SLICE] = state_tasks_model_1.initialTaskState,
    _a[state_feature_account_model_1.FEATURE_ACCOUNT_SLICE] = state_feature_account_model_1.initialFeatureAccountState,
    _a[state_org_model_1.ORG_SLICE] = state_org_model_1.initialOrgState,
    _a);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGUtYXBwLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHF1dXAtbXZwLW54L3NoYXJlZC1hcGktaW50ZXJmYWNlLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9zdGF0ZS1hcHAubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBT0EsNkVBSXVDO0FBQ3ZDLHVFQUdvQztBQUNwQyxxREFBK0Q7QUFDL0QsNkRBQTZFO0FBQzdFLHlEQUFvRTtBQVd2RCxRQUFBLGVBQWU7UUFDMUIsSUFBSSxFQUFFLGtEQUF1QjtRQUM3QixPQUFPLEVBQUUseUNBQW1COztJQUM1QixHQUFDLCtCQUFXLElBQUcsb0NBQWdCO0lBQy9CLEdBQUMsbURBQXFCLElBQUcsd0RBQTBCO0lBQ25ELEdBQUMsMkJBQVMsSUFBRyxpQ0FBZTtRQUM1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gIE9yZ0VudGl0eVN0YXRlLFxuICBReENvdXJzZUFsaWFzRW50aXR5U3RhdGUsXG4gIFF4VGFza0VudGl0eVN0YXRlXG59IGZyb20gJy4uL2VudGl0aWVzJztcbmltcG9ydCB7IENtc09yZyB9IGZyb20gJy4vY21zLW9iamVjdC5tb2RlbCc7XG5pbXBvcnQgeyBJbnRlcmFjdGl2ZUV2ZW50IH0gZnJvbSAnLi9pbnRlcmFjdGl2ZS1ldmVudC5tb2RlbCc7XG5pbXBvcnQge1xuICBGZWF0dXJlQWNjb3VudFN0YXRlLFxuICBGRUFUVVJFX0FDQ09VTlRfU0xJQ0UsXG4gIGluaXRpYWxGZWF0dXJlQWNjb3VudFN0YXRlXG59IGZyb20gJy4vc3RhdGUtZmVhdHVyZS1hY2NvdW50Lm1vZGVsJztcbmltcG9ydCB7XG4gIEZlYXR1cmVBdXRoU3RhdGUsXG4gIGluaXRpYWxGZWF0dXJlQXV0aFN0YXRlXG59IGZyb20gJy4vc3RhdGUtZmVhdHVyZS1hdXRoLm1vZGVsJztcbmltcG9ydCB7IGluaXRpYWxPcmdTdGF0ZSwgT1JHX1NMSUNFIH0gZnJvbSAnLi9zdGF0ZS1vcmcubW9kZWwnO1xuaW1wb3J0IHsgQXBwU2Vzc2lvblN0YXRlLCBpbml0aWFsU2Vzc2lvblN0YXRlIH0gZnJvbSAnLi9zdGF0ZS1zZXNzaW9uLm1vZGVsJztcbmltcG9ydCB7IGluaXRpYWxUYXNrU3RhdGUsIFRBU0tTX1NMSUNFIH0gZnJvbSAnLi9zdGF0ZS10YXNrcy5tb2RlbCc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgQXBwU3RhdGUge1xuICBzZXNzaW9uOiBBcHBTZXNzaW9uU3RhdGU7XG4gIHRhc2tzPzogUXhUYXNrRW50aXR5U3RhdGU7XG4gIGNvdXJzZXM/OiBReENvdXJzZUFsaWFzRW50aXR5U3RhdGU7XG4gIGFjY291bnQ/OiBGZWF0dXJlQWNjb3VudFN0YXRlO1xuICBhdXRoPzogRmVhdHVyZUF1dGhTdGF0ZTtcbiAgb3JnYW5pemF0aW9ucz86IE9yZ0VudGl0eVN0YXRlO1xufVxuXG5leHBvcnQgY29uc3QgaW5pdGlhbEFwcFN0YXRlOiBBcHBTdGF0ZSA9IHtcbiAgYXV0aDogaW5pdGlhbEZlYXR1cmVBdXRoU3RhdGUsXG4gIHNlc3Npb246IGluaXRpYWxTZXNzaW9uU3RhdGUsXG4gIFtUQVNLU19TTElDRV06IGluaXRpYWxUYXNrU3RhdGUsXG4gIFtGRUFUVVJFX0FDQ09VTlRfU0xJQ0VdOiBpbml0aWFsRmVhdHVyZUFjY291bnRTdGF0ZSxcbiAgW09SR19TTElDRV06IGluaXRpYWxPcmdTdGF0ZVxufTtcblxuZXhwb3J0IGludGVyZmFjZSBBcHBSb3V0ZURhdGEge1xuICBvcmc/OiBDbXNPcmc7XG4gIGV2ZW50PzogSW50ZXJhY3RpdmVFdmVudDtcbn1cbiJdfQ==