"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ErrorActionProp = /** @class */ (function () {
    function ErrorActionProp(error, length) {
        if (length === void 0) { length = 0; }
        this.error = error;
        this.length = length;
    }
    return ErrorActionProp;
}());
exports.ErrorActionProp = ErrorActionProp;
function ErrorAction(error, length) {
    if (length === void 0) { length = 0; }
    return new ErrorActionProp(error, length);
}
exports.ErrorAction = ErrorAction;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmdyeC5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BxdXVwLW12cC1ueC9zaGFyZWQtYXBpLWludGVyZmFjZS8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvbmdyeC5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBO0lBQ0UseUJBQW1CLEtBQWEsRUFBUyxNQUFVO1FBQVYsdUJBQUEsRUFBQSxVQUFVO1FBQWhDLFVBQUssR0FBTCxLQUFLLENBQVE7UUFBUyxXQUFNLEdBQU4sTUFBTSxDQUFJO0lBQUcsQ0FBQztJQUN6RCxzQkFBQztBQUFELENBQUMsQUFGRCxJQUVDO0FBRlksMENBQWU7QUFJNUIsU0FBZ0IsV0FBVyxDQUFDLEtBQWEsRUFBRSxNQUFVO0lBQVYsdUJBQUEsRUFBQSxVQUFVO0lBQ25ELE9BQU8sSUFBSSxlQUFlLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0FBQzVDLENBQUM7QUFGRCxrQ0FFQyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBFcnJvckFjdGlvblByb3Age1xuICBjb25zdHJ1Y3RvcihwdWJsaWMgZXJyb3I/OiBFcnJvciwgcHVibGljIGxlbmd0aCA9IDApIHt9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBFcnJvckFjdGlvbihlcnJvcj86IEVycm9yLCBsZW5ndGggPSAwKSB7XG4gIHJldHVybiBuZXcgRXJyb3JBY3Rpb25Qcm9wKGVycm9yLCBsZW5ndGgpO1xufVxuIl19