"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var core_1 = require("@angular/core");
var dismissable_model_1 = require("./dismissable.model");
var DocumentResponse;
(function (DocumentResponse) {
    DocumentResponse["ACCEPT"] = "ok";
    DocumentResponse["REJECT"] = "reject";
})(DocumentResponse = exports.DocumentResponse || (exports.DocumentResponse = {}));
var InteractiveDocument = /** @class */ (function (_super) {
    tslib_1.__extends(InteractiveDocument, _super);
    function InteractiveDocument() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /**
         * All interactive documents are dismissable by default
         */
        _this.dismissable = true;
        /**
         * This event will be emitted when the user responds
         */
        _this.userResponse = new core_1.EventEmitter();
        return _this;
    }
    InteractiveDocument.propDecorators = {
        userResponse: [{ type: core_1.Output }]
    };
    return InteractiveDocument;
}(dismissable_model_1.Dismissable));
exports.InteractiveDocument = InteractiveDocument;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJhY3RpdmUtZG9jLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHF1dXAtbXZwLW54L3NoYXJlZC1hcGktaW50ZXJmYWNlLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9pbnRlcmFjdGl2ZS1kb2MubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsc0NBQXFEO0FBQ3JELHlEQUFrRDtBQUVsRCxJQUFZLGdCQUdYO0FBSEQsV0FBWSxnQkFBZ0I7SUFDMUIsaUNBQWEsQ0FBQTtJQUNiLHFDQUFpQixDQUFBO0FBQ25CLENBQUMsRUFIVyxnQkFBZ0IsR0FBaEIsd0JBQWdCLEtBQWhCLHdCQUFnQixRQUczQjtBQUVEO0lBQWtELCtDQUVqRDtJQUZEO1FBQUEscUVBYUM7UUFWQzs7V0FFRztRQUNILGlCQUFXLEdBQUcsSUFBSSxDQUFDO1FBRW5COztXQUVHO1FBRUgsa0JBQVksR0FBbUMsSUFBSSxtQkFBWSxFQUFFLENBQUM7O0lBQ3BFLENBQUM7OytCQUZFLGFBQU07O0lBRVQsMEJBQUM7Q0FBQSxBQWJELENBQWtELCtCQUFXLEdBYTVEO0FBYnFCLGtEQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBEaXNtaXNzYWJsZSB9IGZyb20gJy4vZGlzbWlzc2FibGUubW9kZWwnO1xuXG5leHBvcnQgZW51bSBEb2N1bWVudFJlc3BvbnNlIHtcbiAgQUNDRVBUID0gJ29rJyxcbiAgUkVKRUNUID0gJ3JlamVjdCdcbn1cblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEludGVyYWN0aXZlRG9jdW1lbnQgZXh0ZW5kcyBEaXNtaXNzYWJsZTxcbiAgSW50ZXJhY3RpdmVEb2N1bWVudFxuPiB7XG4gIC8qKlxuICAgKiBBbGwgaW50ZXJhY3RpdmUgZG9jdW1lbnRzIGFyZSBkaXNtaXNzYWJsZSBieSBkZWZhdWx0XG4gICAqL1xuICBkaXNtaXNzYWJsZSA9IHRydWU7XG5cbiAgLyoqXG4gICAqIFRoaXMgZXZlbnQgd2lsbCBiZSBlbWl0dGVkIHdoZW4gdGhlIHVzZXIgcmVzcG9uZHNcbiAgICovXG4gIEBPdXRwdXQoKVxuICB1c2VyUmVzcG9uc2U6IEV2ZW50RW1pdHRlcjxEb2N1bWVudFJlc3BvbnNlPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbn1cbiJdfQ==