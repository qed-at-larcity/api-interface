"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var app_routes_1 = require("./app.routes");
exports.appFeatureRoutes = [
    {
        path: app_routes_1.AppRoute.PRODUCT,
        loadChildren: '@quup-mvp-nx/feature-product-mvp#FeatureProductMvpModule'
    },
    {
        path: app_routes_1.AppRoute.AUTH,
        loadChildren: '@quup-mvp-nx/shared/feature-auth#FeatureAuthModule'
    },
    /**
     * @TODO design and code the organization context for all routes below
     * this section
     */
    {
        path: app_routes_1.AppRoute.ACCOUNT,
        loadChildren: '@quup-mvp-nx/shared/feature-account#FeatureAccountModule'
    },
    {
        path: app_routes_1.AppRoute.EVENTS,
        loadChildren: '@quup-mvp-nx/feature-events#FeatureEventsModule'
    }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmVhdHVyZXMucm91dGVzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHF1dXAtbXZwLW54L3NoYXJlZC1hcGktaW50ZXJmYWNlLyIsInNvdXJjZXMiOlsibGliL3JvdXRlcy9mZWF0dXJlcy5yb3V0ZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFDQSwyQ0FBd0M7QUFFM0IsUUFBQSxnQkFBZ0IsR0FBVztJQUN0QztRQUNFLElBQUksRUFBRSxxQkFBUSxDQUFDLE9BQU87UUFDdEIsWUFBWSxFQUFFLDBEQUEwRDtLQUN6RTtJQUNEO1FBQ0UsSUFBSSxFQUFFLHFCQUFRLENBQUMsSUFBSTtRQUNuQixZQUFZLEVBQUUsb0RBQW9EO0tBQ25FO0lBQ0Q7OztPQUdHO0lBQ0g7UUFDRSxJQUFJLEVBQUUscUJBQVEsQ0FBQyxPQUFPO1FBQ3RCLFlBQVksRUFBRSwwREFBMEQ7S0FDekU7SUFDRDtRQUNFLElBQUksRUFBRSxxQkFBUSxDQUFDLE1BQU07UUFDckIsWUFBWSxFQUFFLGlEQUFpRDtLQUNoRTtDQUNGLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSb3V0ZXMgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgQXBwUm91dGUgfSBmcm9tICcuL2FwcC5yb3V0ZXMnO1xuXG5leHBvcnQgY29uc3QgYXBwRmVhdHVyZVJvdXRlczogUm91dGVzID0gW1xuICB7XG4gICAgcGF0aDogQXBwUm91dGUuUFJPRFVDVCxcbiAgICBsb2FkQ2hpbGRyZW46ICdAcXV1cC1tdnAtbngvZmVhdHVyZS1wcm9kdWN0LW12cCNGZWF0dXJlUHJvZHVjdE12cE1vZHVsZSdcbiAgfSxcbiAge1xuICAgIHBhdGg6IEFwcFJvdXRlLkFVVEgsXG4gICAgbG9hZENoaWxkcmVuOiAnQHF1dXAtbXZwLW54L3NoYXJlZC9mZWF0dXJlLWF1dGgjRmVhdHVyZUF1dGhNb2R1bGUnXG4gIH0sXG4gIC8qKlxuICAgKiBAVE9ETyBkZXNpZ24gYW5kIGNvZGUgdGhlIG9yZ2FuaXphdGlvbiBjb250ZXh0IGZvciBhbGwgcm91dGVzIGJlbG93XG4gICAqIHRoaXMgc2VjdGlvblxuICAgKi9cbiAge1xuICAgIHBhdGg6IEFwcFJvdXRlLkFDQ09VTlQsXG4gICAgbG9hZENoaWxkcmVuOiAnQHF1dXAtbXZwLW54L3NoYXJlZC9mZWF0dXJlLWFjY291bnQjRmVhdHVyZUFjY291bnRNb2R1bGUnXG4gIH0sXG4gIHtcbiAgICBwYXRoOiBBcHBSb3V0ZS5FVkVOVFMsXG4gICAgbG9hZENoaWxkcmVuOiAnQHF1dXAtbXZwLW54L2ZlYXR1cmUtZXZlbnRzI0ZlYXR1cmVFdmVudHNNb2R1bGUnXG4gIH1cbl07XG4iXX0=